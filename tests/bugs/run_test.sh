#!/bin/sh
FUNK_MACHINE=../../src/core/funky

for theTest in `ls *.funk`; do
	OUTY=`echo $theTest | sed -e 's/.funk$/.out/g'`
	EXPECT=`echo $theTest | sed -e 's/.funk$/.expected/g'`
	$FUNK_MACHINE -f $theTest | grep -ve '### Funky' | sed -e 's/ *$//' > /tmp/$OUTY
	diff /tmp/$OUTY $EXPECT
        if [ "$?" != "0" ]; then
            echo "$theTest FAILED!" >&2
            exec false
        fi
	echo "$theTest OK"
done

