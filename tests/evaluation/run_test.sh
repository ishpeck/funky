#!/bin/sh
FUNK_MACHINE=../../src/core/funky

for theTest in `ls *.funk`; do
	OUTY="/tmp/`echo $theTest | sed -e 's/.funk$/.out/g'`"
	EXPECT=`echo $theTest | sed -e 's/.funk$/.expected/g'`
	$FUNK_MACHINE -f $theTest | grep -ve '### Funky' | sed -e 's/ *$//' > $OUTY
	diff $OUTY $EXPECT
        if [ "$?" != "0" ]; then
            echo "$theTest FAILED!" >&2
            exec false
        fi
	echo "$theTest OK"
done

echo '(* 3 0.33333)' | $FUNK_MACHINE | grep -e 'number 99*[0-9]* 100*' >/dev/null || exec false
