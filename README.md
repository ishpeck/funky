Funky is a lisp-like scripting language.

# Quick Start

You'll need to have the BoehmGC garbage collector library.  It's
usually available as a package on most platforms/distributions.  If
not, it's easy enough to compile and is available at this web site:

 - [https://www.hboehm.info/gc/](https://www.hboehm.info/gc/)

Once that's installed, you only need `make` and a compiler like
`clang` or `GCC`

```
$ git clone https://gitlab.com/ishpeck/funky.git
$ cd funky/src/core
$ make
echo '(+ 3 2 1)' | ./funk_machine
```

# Intro

For the most part, Funky is just a Lisp clone with some ideas pilfered
from other languages.

## Procedure Definition

Defining procedures looks similar to Common Lisp:

```
#> (def foo (x y) (+ x y))
#> (foo 3 4)
7
```

Naming a procedure  is optional.  If you omit the  procedure name in a
def call, it becomes an anonymous procedure (a lambda).

```
#> ((def (x y) (+ x y)) 3 4)
7
```

## Macro Definition

Macros should come as no shock to anybody accustomed to Lisp-like
languages.

```
#> (mac together (x y) (append x y))
#> (together (1 2 3) (4 5 6))
(1 2 3 4 5 6)
```
Macros can be anonymous:

```
#> ((mac (x y) (append x y)) (1 2 3) (4 5 6))
(1 2 3 4 5 6)
```

## List Construction and Sugar

The `list` procedure generates a list of its arguments...

```
#> (list 9 8 7)
(9 8 7)
```

Square brackets in the form of `[...]` are the same as calling the
`(list ...)` procedure.

```
#> [1 2 3]
(1 2 3)
```

Commas are white-space characters in Funky.  This has the side-effect
that people coming from Python or Haskell or Javascript could
construct lists according to their customs...

```
#> [1,2,3]
(1 2 3)
```

## Pairs (conses), and Jigs (dotted cons)

The `cons` procedure found in most Lisp-like languages is called
`pair` in Funky.

```
#> (pair 1 (pair 2 (pair 3 nil)))
(1 2 3)
```

There is no syntax for the "dotted cons" as found in other Lisps.  The
period token is used for something else.  You can use the `pair`
procedure to construct one.

The `:` (aka: "jig") operator is short-hand for constructing a pair of
the expressions found on either side of it...

```
#> "foo":123
(pair "foo" 123)
#> 1:[2 3 4]
(1 2 3 4)
```

## Grids

Grids are associative lists...

```
(grid (pair "key" "val") (pair "one" 1))
```

Funky's syntax was meant to make natively consuming JSON and reading
it as a `grid` object easy...

```
#> {"key" : "val", "one" : 1}
(grid (pair "key" "val") (pair "one" 1))
```

Elements from a `grid` can be extracted with the `get` procedure:

```
#> (get {foo:3} foo)
3
```

The `.` operator is syntactic sugar which identical to the `get`
procedure -- treating its left operand as the `grid` and its right
operand as the key.

```
#> {foo:3}.foo
3
```

The `.` operator works even if there is whitespace between it and its
operands:

```
#> {foo:3} . foo
3
```

## set!

The `set!` macro assigns variables.  The exclamation mark is
convention for "this is stateful and therefore dangerous."

```
#> (set! x 77)
77
#> x
77
#> (+ x 3)
80
```

## Comments

If your code is ugly and complex enough to justify comments, use the
`#` token to comment out everything until the end of the line (just
like in shell scripting languages or in Python).

```
#> # this does absolutely nothing whatsoever
```

You may have noticed that the REPL prompt `#>` begins with the `#`
token.  This has the side effect of being able to copy/paste the
terminal content from your REPL session and paste it into a new REPL
session -- having the entire pasted content evaluate harmlessly in the
funk machine.

## Arity, Partial and More

Procedures defined with `def` are variatic (can take any number of
arguments).  If the procedure is defined as taking more arguments than
the number provided at call time, it evaluates down to a new procedure
called a "partial" which asks for the arguments not supplied.

This is a behavior called "implicit partialing."

```
#> (def foo (x y z) (+ x y z))
#> (foo 1 2 3)
6
#> (foo 3)
(def (y z) (foo 3 y z))
#> (set! threeplus (foo 3))
(def (y z) (foo 3 y z))
#> (threeplus 4 5)
14
```

If you pass in more arguments than the function is asking for, the
extra arguments get kept as a list which can be referenced in the
function body by calling the `more-args` procedure...

```
#> (def bar (x y) (more-args))
#> (bar 3 4 5 6 7 8 9)
(5 6 7 8 9)
```

## Other Handy Junk

The `seq` procedure generates a sequence of numbers starting with the
first argument and ending with the second.

```
#> (seq 1 20)
(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20)
```

Some of the higher order functions found in other Lisps are available
here:

```
#> (def plusTwo (x) (+ x 2))
#> (map plusTwo (seq 1 10))
(3 4 5 6 7 8 9 10 11 12)
```

The `fold` procedure is called `reduce` in other Lisps:

```
#> (fold + (seq 1 5))
15
```

## Application Sugar

Sometimes people are intimidated by the number of parenthesis they may
end up with in their Lisp code.  Funky adds some very non-Lispy syntax
to the mix to reduce the paren strain.

The `->` token stands for "pass into" and passes the expression on its
left into the procedure on its right...

```
#> (def plusThree (x) (+ x 3))
#> 4 -> plusThree 
7
#> # "4 -> plusThree" is identical to (plusThree 4)
```

The `<-` token stands for "receives" and reverses the order: Passing
the expression on its right as the argument to the procedure on its
left.

```
#> plusThree <- 9
12
```

The `~>` token stands for "as args for" and passes each element of the
list on the left as arguments into the procedure on the right.  It's
sugar for `(apply procname listname)` as follows:

```
#> (def sumDoubles (x y z) (apply + (map (def (a) (* 2 a)) [x y z])))
#> [1,2,3] ~> sumDoubles
12
```

And `<~` reverses the order, applying the procedure on the left to the
list on the right:

```
#> sumDoubles <~ [2, 3, 4]
18
```

This sugar, when combined with implicit partialing, can chain break
deeply nested expressions into small, legible parts:

```
#> (seq 1 10) -> (map (def (x) (+ x 3))) ~> +
85
```

## Laziness

Funky has "generators" similar to those found in other languages.
These can be used to lazily evaluate.

The `repeat` procedure lazily generates an infinite list of its given
expression.  `(take n l)` will evaluate to the first `n` arguments of
the list `l` and discard the rest.

```
#> (+ 3 2) -> repeat -> (map print) -> (take 4)
5
5
5
5
5
(5 5 5 5 5)
```

The `finalize` procedure can be used to force generated code to
evaluate.

```
#> (seq 1 100) -> (map print) -> (take 4)
1
2
3
4
(1 2 3 4)
#> (seq 1 20) -> (map print) -> finalize -> (take 4)
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
(1 2 3 4)
```

So if you want to create an infinite loop that does nothing...

```
#> (repeat nil) -> finalize
```
 
# Documentation

The most unambiguous documentation for this language will always be in
the tests.  Look  at src/test/*.in and the related *.out  files to see
how everything is generally expected to work.  Plan is to (eventually)
generate docs from the test data. :P

# Gotchas

Some things you'll want to know:

  1. We don't  (and probably won't ever)  use the ' token  to quote an
     expression.  Instead,  the single  quote character behaves  as it
     does in most shell  scripting languages; signifying the beginning
     and end of a quoted  string.  (Just like double-quote does).  You
     can use the quote macro to quote things instead: (quote foo)
  2. We  use a "true" keyword  instead of "t" to  represent truthiness
     because single-character global constants are stupid.
  3. The funk machine currently  reads until EOF before evaluating the
     text you gave it.  This is dumb and should change soon.
  4. Not all of the necessary features are finished
  5. It's slow/inefficient.  It'll be faster.  Promise.
  6. The  "car" operator is called  "head" and "cdr" is  called "rest"
     because that cryptic  tradition isn't very helpful.   You can try
     to (set! car head) if you absolutely can't let those terms go.
  7. If you're used to Haskell, you  may notice that the : operator is
     similar  to  Haskell's.   Be  warned:   Funky  does  not  do  the
     evaluation order shenanigans that Haskell does and so will always
     evaluate left to right.  So where haskell will say "1:2:[3,4]" is
     equal to  a list of "[1,2,3,4]"  Funky will have that  exact same
     input result in  a lisp-style "dotted cons" being  the very first
     element in a list: "((pair 1 2) 3 4)"

# License

Copyright (c) 2013, Anthony "Ishpeck" Tedjamulia All rights reserved.

Redistribution  and use  in source  and other  forms, with  or without
modification, are permitted provided that the following conditions are
met:

Redistributions of source code must retain the above copyright notice,
this list of conditions  and the following disclaimer. Redistributions
in other form must reproduce the  above copyright notice, this list of
conditions and  the following  disclaimer in the  documentation and/or
other materials  provided with the  distribution. Neither the  name of
the author nor the names of its contributors may be used to endorse or
promote  products derived  from this  software without  specific prior
written permission. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS
AND  CONTRIBUTORS  "AS IS"  AND  ANY  EXPRESS OR  IMPLIED  WARRANTIES,
INCLUDING,   BUT   NOT  LIMITED   TO,   THE   IMPLIED  WARRANTIES   OF
MERCHANTABILITY   AND   FITNESS   FOR   A   PARTICULAR   PURPOSE   ARE
DISCLAIMED. IN NO EVENT SHALL  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR  ANY DIRECT,  INDIRECT, INCIDENTAL, SPECIAL,  EXEMPLARY, OR
CONSEQUENTIAL DAMAGES  (INCLUDING, BUT NOT LIMITED  TO, PROCUREMENT OF
SUBSTITUTE  GOODS OR  SERVICES;  LOSS  OF USE,  DATA,  OR PROFITS;  OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND  ON ANY THEORY OF LIABILITY,
WHETHER IN  CONTRACT, STRICT LIABILITY, OR  TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
