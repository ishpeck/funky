# Package Management

Funky's package management should satisfy the following ideals:

## User Trust Via Signatures

In order to participate in the package community, you will need a
signify key that you emit as being your own.  You will sign things as
having your approval.  Those users who trust you will look out for the
things you sign as good.

The package system should be able to sign packages with the following
meanings:

 - Package passes the following tests (lists tests) on the following
   platform (name platform) and Funk Machine (list Funky version)
 - Tests obviated by this other set of tests (lists tests)

## Test-Based Package Description

Tests, more than code, should be the means of locating packages.
Users should not attempt to submit a piece of Funk and say "this
solves problem X."  Instead, they should present to the community,
"This is how I define problem X."  After generating a SHA sum of the
tests you provide, you may emit a solution; a piece of funk that
satisfies that test.

When searching for packages, you will search the _test_ database for
descriptions that match what you're after.  The test database will
show a list of users who sign-off on that test.  This gives you a
sense of trust for the kinds of software that would pass these tests.

As a community, Funky will value test contributions more than code
contributions.

When you find a set of problems that match your own, you'll be
presented with a series of packages that also pass those tests as well
as a set of users who sign-off on the solutions saying "I observed
this package passing these tests."

The slogan states: "If it's not worth testing, it's not worth
distributing."  It's a condemnation of the npm way (where every
idiotic use-case gets distributed as a way to do things) and can force
users to think about what they're actually creating when they want to
create a package.

## Unique Identifiers

Otherwise known as: Idiotic names should be repressed.  If you leave
the world to name packages, they'll be shit-heads about it and name
them crap like "meow" and "liz."

Any remote package should be referenced by the `SHA256` sum of its
tests and code that passes those tests.  This ensures the name's
uniqueness _and_ the version of the package at the same time.  It also
allows us to have a trustworthy, distributed package hosting service.
Packages can exist anywhere and you know the contents are good by
validating the SHA sum of package's downloaded contents.

Variations on tests can be signed by users as obviating other sets of
tests.  The community will be able to debate on the substance of a
problem definition more than they will on the aesthetics of a
solution in this way.

## Bundle Your Deps

Other package systems will often dep-in the world.  In many package
managers, mutually conflicting versions of the same dependencies will
cause problems.

The npm world solves this by deping in not just the world but each
version of each component thereof.  If the dependencies are also
loosely versioned and bugs creep in at some point, the package can
behave in ways that don't match the maintainer's tests.

The solution is, whenever a package is built, if it's at all possible,
replicate the dependency code and ship it as a single package.  This
makes all users of a package actual forks of the same.  If your goal
is to minimize redundancy in code, this looks horrendous.  If your
goal is to maximize the chances that a package, once tested, is
reliable forever after, it's sound.

This is essentially the same as "statically linking" binaries in the
natively-compiled programming world.  You're bundling the whole
package with everything it needs to run --- not trusting any external
components to solve the problem for you.
