#include "funky.h"

REPR_BLDR(Primordial_Atom,atom_th,atom_k)
REPR_BLDR(Primordial_Number,number_th,number_k)
REPR_BLDR(Primordial_String,string_th,string_k)

REPR_DSTR(del_atom,atom_th)
REPR_DSTR(del_number,number_th)
REPR_DSTR(del_string,string_th)

LST_BLDR(Primordial_Cons,cons_th,cons_k)
LST_BLDR(Primordial_Proc,procedure_th,procedure_k)
LST_BLDR(Primordial_Mac,macro_th,macro_k)
LST_BLDR(Primordial_Gen,gen_th,gen_k)

LST_DSTR(del_cons,cons_th)
LST_DSTR(del_err,err_th)
LST_DSTR(del_proc,procedure_th)
LST_DSTR(del_mac,macro_th)
LST_DSTR(del_gen,gen_th)


cell_th *Atom(const char *label) {
    return Primordial_Atom(label, GC_REGISTER);
}

cell_th *Number(const char *label) {
    return Primordial_Number(label, GC_REGISTER);
}

cell_th *String(const char *label) {
    return Primordial_String(label, GC_REGISTER);
}

cell_th *Cons(cell_th *car, cell_th *cdr) {
    return Primordial_Cons(car, cdr, GC_REGISTER);
}

cell_th *Proc(cell_th *car, cell_th *cdr) {
    return Primordial_Proc(car, cdr, GC_REGISTER);
}

cell_th *Mac(cell_th *car, cell_th *cdr) {
    return Primordial_Mac(car, cdr, GC_REGISTER);
}

cell_th *Gen(cell_th *car, cell_th *cdr) {
    return Primordial_Gen(car, cdr, GC_REGISTER);
}

cell_th *Primordial_Err(cell_th *messages, int shallReg) {
    err_th *this_error=calloc(1, sizeof(err_th));
    this_error->kind=error_k;
    this_error->CAR=Atom("err");
    this_error->CDR=messages;
    return shallReg ? reg_thing((cell_th *)this_error) : (cell_th *)this_error;
}

cell_th *Err(cell_th *messages) {
    fprintf(stderr, "Error: ");
    depict_error(messages);
    return Primordial_Err(messages, GC_REGISTER);
}

cell_th *Primordial_Routine(c_routine theFunction, int shallReg) {
    routine_th *rout=calloc(1, sizeof(routine_th));
    rout->kind=routine_k;
    rout->routine=theFunction;
    return shallReg ? reg_thing((cell_th *)rout) : (cell_th *)rout;
}

cell_th *Routine(c_routine theFunction) {
    return Primordial_Routine(theFunction, GC_REGISTER);
}

cell_th *Primordial_CVau(c_routine theFunction, int shallReg) {
    routine_th *rout=calloc(1, sizeof(routine_th));
    rout->kind=method_k;
    rout->routine=theFunction;
    return shallReg ? reg_thing((cell_th *)rout) : (cell_th *)rout;
}

cell_th *CVau(c_routine theFunction) {
    return Primordial_CVau(theFunction, GC_REGISTER);
}

stream_kind stream_type(cell_th *thing) {
    stream_th *stream;
    if(th_kind(thing)!=stream_k)
        return stream_broken;
    stream=(stream_th *)thing;
    return stream->streamKind;
}

static cell_th *Primordial_Stream(stream_kind knd,
                            stream_mode mde,
                            FILE *streamData,
                            int shallReg) {
    stream_th *stream=calloc(1, sizeof(stream_th));
    stream->kind=stream_k;
    stream->streamKind=knd;
    stream->mode=mde;
    stream->data=streamData;
    return shallReg ? reg_thing((cell_th *)stream) : (cell_th *)stream;
}

static const char *stream_mode_txt(stream_mode mode) {
    switch(mode) {
        case mode_read:
            return "r";
        case mode_write:
            return "w";
        case mode_append:
            return "a";
    }
    return "r";
}

cell_th *F_Stream(const char *path, stream_mode mode) {
    FILE *nf=fopen(path, stream_mode_txt(mode));
    if(!nf)
        return Err(Cons(String("Unable to open file."), 
                        Cons(String(path), NULL)));
    return Primordial_Stream(stream_on_platter, 
                             mode, nf, GC_REGISTER);
}

cell_th *D_Stream(int fdnum, stream_mode mode) {
    FILE *nf=fdopen(fdnum, stream_mode_txt(mode));
    if(!nf)
        return Err(Cons(String("Unable to open file descriptor"), NULL));
    return Primordial_Stream(stream_descriptor,
                             mode, nf, GC_REGISTER);
}

int del_routine(cell_th *thing) {
    routine_th *rout=(routine_th *)thing;
    rout->routine=NULL;
    free(rout);
    return 0;
}

int del_method(cell_th *thing) {
    method_th *mthd=(method_th *)thing;
    mthd->routine=NULL;
    free(mthd);
    return 0;
}

int del_stream(cell_th *thing) {
    stream_th *stream=(stream_th *)thing;
    if(stream->data) {
        switch(stream->streamKind) {
            case stream_descriptor:
            case stream_on_platter:
                fclose(stream->data);
                break;
            case stream_sub_process:
                pclose(stream->data);
                break;
            case stream_broken:
                break;
        }
    }
    stream->data=NULL;
    stream->streamKind=stream_broken;
    free(stream);
    return 0;
}

cell_th *Primordial_Grid(int shallReg) {
    grid_th *grid=calloc(1, sizeof(grid_th));
    grid->kind=grid_k;
    grid->data=new_grid();
    return shallReg ? reg_thing((cell_th *)grid) : (cell_th *)grid;
}

cell_th *Grid(void) {
    return Primordial_Grid(GC_REGISTER);
}

int del_grid(cell_th *grid) {
    grid_th *grd=(grid_th *)grid;
    wipe_grid(grd->data);
    free(grd);
    return 0;
}

kind_t th_kind(const cell_th *thing) {
    if(!thing)
        return null_k;
    return thing->kind;
}

int is_atom(const cell_th *thing) {
    return th_kind(thing)==atom_k;
}

int is_number(const cell_th *thing) {
    return th_kind(thing)==number_k;
}

int is_gen(const cell_th *thing) {
    return th_kind(thing)==gen_k;
}

int is_list(const cell_th *thing) {
    switch(th_kind(thing)) {
        case cons_k: case error_k: case procedure_k:
        case macro_k: case gen_k:
            return FUNKY_AFFIRMATIVE;
        default: 
            return FUNKY_NEGATIVE;
    }
}

int is_lambda(const cell_th *thing) {
    switch(th_kind(thing)) {
        case procedure_k: case routine_k: case macro_k:
        case method_k:
            return FUNKY_AFFIRMATIVE;
        default:
            return FUNKY_NEGATIVE;
    }
}

int is_grid(const cell_th *thing) {
    return th_kind(thing)==grid_k;
}

int is_error(const cell_th *thing) {
    return th_kind(thing)==error_k;
}

int is_string(const cell_th *thing) {
    return th_kind(thing)==string_k;
}

int is_functor(const cell_th *thing) {
    switch(th_kind(thing)) {
        case method_k: case macro_k:
            return FUNKY_AFFIRMATIVE;
        default: 
            return FUNKY_NEGATIVE;
    }
}

int has_repr(const cell_th *thing) {
    switch(th_kind(thing)) {
        case atom_k: case number_k: case string_k:
            return FUNKY_AFFIRMATIVE;
        default:
            return FUNKY_NEGATIVE;
    }
}

static char *th_repr(const cell_th *thing) {
    atom_th *toRepr=(atom_th *)thing;
    return toRepr->repr;
}

const char *sym(const cell_th *thing) {
    switch(th_kind(thing)) {
        case atom_k:
        case number_k:
        case string_k:
            return th_repr(thing);
        case error_k:
            return "(error";
        case cons_k:
        case procedure_k:
        case macro_k:
        case gen_k:
            return "(";
        case grid_k:
            return "{";
        case routine_k:
            return "(routine";
        case method_k:
            return "(method";
        case null_k:
            return "nil";
        case stream_k:
            return "(stream)";
    }
    return "<thing>";
}

FILE *stream_source(cell_th *thing) {
    stream_th *stream;
    if(th_kind(thing)!=stream_k)
        return NULL;
    stream=(stream_th *)thing;
    return stream->data;
}

int set_stream_kind(cell_th *thing, stream_kind knd) {
    stream_th *stream;
    if(th_kind(thing)!=stream_k)
        return 1;
    stream=(stream_th *)thing;
    stream->streamKind=knd;
    return 0;
}

static cell_th *str_first_char(string_th *string) {
    char *character;
    cell_th *atom;
    if(!string->repr)
        return NULL;
    asprintf(&character, "%c", string->repr[0]);
    atom=String(character);
    erase_string(character);
    return atom;
}

static cell_th *str_remaining_text(string_th *string) {
    char *text;
    cell_th *atom;
    if(!string->repr || strlen(string->repr)<2)
        return NULL;
    asprintf(&text, "%s", string->repr+1);
    atom=String(text);
    erase_string(text);
    return atom;
}

static cell_th *cons_first_thing(const cons_th *cons) {
    if(!cons)
        return NULL;
    return cons->CAR;
}

static cell_th *cons_second_thing(const cons_th *cons) {
    if(!cons)
        return NULL;
    return cons->CDR;
}

cell_th *Car(const cell_th *thing) {
    switch(th_kind(thing)) {
        case atom_k:
        case number_k:
        case method_k:
        case routine_k:
        case stream_k:
            return NULL;
        case string_k:
            return str_first_char((string_th *)thing);
        case error_k:
        case cons_k:
        case procedure_k:
        case macro_k:
        case gen_k:
            return cons_first_thing((cons_th *)thing);
        case grid_k:
            return Keys(thing);
        case null_k:
            return NULL;
    }
    return NULL;
}

cell_th *Cdr(const cell_th *thing) {
    switch(th_kind(thing)) {
        case string_k:
            return str_remaining_text((string_th *)thing);
        case atom_k:
        case number_k:
        case method_k:
        case routine_k:
        case stream_k:
            return NULL;
        case error_k:
        case cons_k:
        case procedure_k:
        case macro_k:
        case gen_k:
            return cons_second_thing((cons_th *)thing);
        case grid_k:
            return Vals(thing);
        case null_k:
            return NULL;
    }
    return NULL;
}

static c_routine extract_routine(routine_th *rt) {
    return rt->routine;
}

c_routine call_rt(cell_th *thing) {
    switch(th_kind(thing)) {
        case method_k: case routine_k:
            return extract_routine((routine_th *)thing);
        default:
            return NULL;
    }
}

int del_thing(cell_th *thing) {
    switch(th_kind(thing)) {
        case atom_k: return del_atom(thing);
        case number_k: return del_number(thing);
        case routine_k: return del_routine(thing);
        case method_k: return del_method(thing);
        case string_k: return del_string(thing);
        case error_k: return del_err(thing);
        case cons_k: return del_cons(thing);
        case procedure_k: return del_proc(thing);
        case macro_k: return del_mac(thing);
        case grid_k: return del_grid(thing);
        case gen_k: return del_gen(thing);
        case stream_k: return del_stream(thing);
        case null_k: return 1;
    }
    return 1;
}

static cell_th *set_cons_cdr(cons_th *cons, cell_th *cdr) {
    cons->CDR=cdr;
    return cdr;
}

static cell_th *set_cons_car(cons_th *cons, cell_th *car) {
    cons->CAR=car;
    return car;
}

cell_th *set_cdr(cell_th *cell, cell_th *cdr) {
    if(!cell || !is_list(cell))
        return NULL;
    return set_cons_cdr((cons_th *)cell, cdr);
}

cell_th *set_car(cell_th *cell, cell_th *car) {
    if(!is_list(cell))
        return NULL;
    return set_cons_car((cons_th *)cell, car);
}

static cell_th *grid_set(grid_th *grid, const char *kw, cell_th *val) {
    set_grid_item(grid->data, kw, (void *)val);
    return val;
}

static cell_th *grid_get(grid_th *grid, const char *kw) {
    return (cell_th *)get_grid_item(grid->data, kw);
}

cell_th *Set(cell_th *grid, const char *kw, cell_th *val) {
    if(!grid || th_kind(grid)!=grid_k)
        return NULL;
    return grid_set((grid_th *)grid, kw, val);
}

cell_th *Get(cell_th *grid, const char *kw) {
    if(!grid || th_kind(grid)!=grid_k)
        return NULL;
    return grid_get((grid_th *)grid, kw);
}

int grid_has(grid_th *grid, const char *kw) {
    return grid_key_exists(grid->data, kw);
}

int Has(cell_th *grid, const char *kw) {
    if(!grid || th_kind(grid)!=grid_k) 
        return 0;
    return grid_has((grid_th *)grid, kw);
}

cell_th *last_el(cell_th *thing) {
    if(!thing || !is_list(thing))
        return NULL;
    while(thing && is_list(Cdr(thing)))
        thing=Cdr(thing);
    return thing;
}

cell_th *append(cell_th *left, cell_th *right) {
    if(!left || !is_list(left))
        return NULL;
    set_cdr(last_el(left), right);
    return left;
}

static cell_th *grid_keys(grid_th *grid) {
    char **keys=grid_keys_list(grid->data);
    char **kw=keys;
    cell_th *allKeys;
    if(!keys)
        return NULL;
    if(!*keys) {
        wipe_keys_list(keys);
        return NULL;
    }
    allKeys=Cons(Atom(*kw++), NULL);
    while(kw && *kw) {
        append(allKeys, Cons(Atom(*kw), NULL));
        kw++;
    }
    wipe_keys_list(keys);
    return allKeys;
}

cell_th *Keys(const cell_th *grid) {
    if(!grid || th_kind(grid)!=grid_k)
        return NULL;
    return grid_keys((grid_th *)grid);
}

static cell_th *grid_vals(grid_th *grid) {
    cell_th *keys=grid_keys(grid);
    cell_th *values;
    if(!keys)
        return NULL;
    values=Cons(grid_get(grid, sym(Car(keys))), NULL);
    keys=Cdr(keys);
    while(keys) {
        append(values, Cons(grid_get(grid, sym(Car(keys))), NULL));
        keys=Cdr(keys);
    }
    return values;
}

cell_th *Vals(const cell_th *grid) {
    if(!grid || th_kind(grid)!=grid_k) 
        return NULL;
    return grid_vals((grid_th *)grid);
}

cell_th *insert(cell_th *left, cell_th *right) {
    cell_th *tmp;
    if(!left || !is_list(left))
        return NULL;
    if(!is_list(right))
        right=Cons(right, NULL);
    tmp=Cdr(left);
    set_cdr(left, append(right, tmp));
    return right;
}

cell_th *accumulate(cell_th *thing) {
    cell_th *accum=Cons(thing, NULL);
    cell_th *cur=accum;
    while(cur) {
        cell_th *item=Car(cur);
        if(th_kind(item)==grid_k)
            insert(cur, Vals(item));
        if(Cdr(item))
            insert(cur, Cons(Cdr(item), NULL));
        if(Car(item))
            insert(cur, Cons(Car(item), NULL));
        cur=Cdr(cur);
    }
    return accum;
}

static cell_th *duplicate_grid(cell_th *gridsrc) {
    cell_th *keys=Keys(gridsrc);
    cell_th *ng=Grid();
    while(keys) {
        Set(ng, sym(Car(keys)), Get(gridsrc, sym(Car(keys))));
        keys=Cdr(keys);
    }
    return ng;
}

static cell_th *dup_cell(cell_th *thing) {
    switch(th_kind(thing)) {
        case number_k:
            return Number(sym(thing));
        case string_k:
            return String(sym(thing));
        case atom_k:
            return Atom(sym(thing));
        case cons_k:
            return Cons(Car(thing), Cdr(thing));
        case error_k:
            return Primordial_Err(Cdr(thing), GC_REGISTER);
        case procedure_k:
            return Proc(Car(thing), Cdr(thing));
        case macro_k:
            return Mac(Car(thing), Cdr(thing));
        case gen_k:
            return Gen(Car(thing), Cdr(thing));
        case routine_k:
            return Routine(call_rt(thing));
        case method_k:
            return CVau(call_rt(thing));
        case grid_k:
            return duplicate_grid(thing);
        case stream_k:
            return thing;
        case null_k:
            return NULL;
    }
}

static cell_th *inner_dup(cell_th *head) {
    if(!head)
        return NULL;
    if(!is_list(head))
        return head;
    if(Car(head))
        set_car(head, dup_cell(Car(head)));
    if(Cdr(head))
        set_cdr(head, dup_cell(Cdr(head)));
    inner_dup(Car(head));
    inner_dup(Cdr(head));
    return head;
}

cell_th *duplicate(cell_th *head) {
    return inner_dup(dup_cell(head));
}
