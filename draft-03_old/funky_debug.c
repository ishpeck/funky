#include "funky.h"

static cell_th *inner_depict(FILE *tgtStream, cell_th *thing);

static int depict_grid_elements(FILE *tgtStream, cell_th *src) {
    cell_th *keywords=Keys(src);
    cell_th *keys=keywords;
    fprintf(tgtStream, "{");
    while(keys) {
        fprintf(tgtStream, "\"%s\": ", 
               sym(Car(keys)));
        inner_depict(tgtStream, Get(src, sym(Car(keys))));
        keys=Cdr(keys);
        if(keys)
            fprintf(tgtStream, ",\n ");
    }
    fprintf(tgtStream, "}");
    return 0;
}

static int depict_routine(FILE *tgtStream, cell_th *rooteen) {
    fprintf(tgtStream, "<c_routine @%d@>", (int)rooteen);
    return 0;
}

static cell_th *depict_lambda(FILE *tgtStream, const char *lbl, cell_th *lambda) {
    cell_th *exprs=Cdr(lambda);
    fprintf(tgtStream, "%s ", lbl);
    if(!Car(lambda))
        fprintf(tgtStream, "()");
    else
        inner_depict(tgtStream, Car(lambda));
    while(exprs) {
        fprintf(tgtStream, " ");
        inner_depict(tgtStream, Car(exprs));
        exprs=Cdr(exprs);
    }
    return Cdr(Cdr(lambda));
}

static cell_th *depict_pair(FILE *tgtStream, cell_th *thing) {
    fprintf(tgtStream, "pair ");
    inner_depict(tgtStream, Car(thing));
    fprintf(tgtStream, " ");
    inner_depict(tgtStream, Cdr(thing));
    return NULL;
}

static cell_th *resolve_for_depiction(FILE *tgtStream, cell_th *gens) {
    cell_th *head;
    cell_th *cur;
    if(!gens)
        return NULL;
    new_env();
    head=Cons(Car(gens), NULL);
    cur=head;
    while(gens) {
        cur=set_cdr(cur, apply(Cdr(gens)));
        gens=cur;
    }
    cur=head;
    fprintf(tgtStream, "(");
    while(cur) {
        inner_depict(tgtStream, Car(cur));
        cur=Cdr(cur);
        if(cur)
            fprintf(tgtStream, " ");
    }
    fprintf(tgtStream, ")");
    pop_env();
    return NULL;
}

static cell_th *inner_depict(FILE *tgtStream, cell_th *thing) {
    int withinList=thing && is_list(thing) && !is_gen(thing);
    cell_th *last=NULL;
    if(!thing) {
        fprintf(tgtStream, "nil");
        return NULL;
    }
    if(withinList)
        fprintf(tgtStream, "(");
    while(thing) {
        switch(th_kind(thing)) {
            case string_k:
                fprintf(tgtStream, "\"%s\"", ((string_th *)thing)->repr);
                thing=NULL;
                break;
            case routine_k: case method_k:
                depict_routine(tgtStream, thing);
                break;
            case procedure_k:
                thing=depict_lambda(tgtStream, "def", thing);
                break;
            case macro_k: 
                thing=depict_lambda(tgtStream, "mac", thing);
                break;
            case gen_k: 
                thing=resolve_for_depiction(tgtStream, thing);
                break;
            case error_k:
                inner_depict(tgtStream, Car(thing));
                break;
            case cons_k:
                if(th_kind(Cdr(thing))!=cons_k && 
                   th_kind(Cdr(thing))!=null_k) 
                    thing=depict_pair(tgtStream, thing);
                else
                    inner_depict(tgtStream, Car(thing));
                break;
            case grid_k:
                depict_grid_elements(tgtStream, thing);
                thing=NULL;
                break;
            case atom_k: case number_k: default:
                fprintf(tgtStream, "%s", sym(thing));
                break;
        }
        last=thing;
        thing=Cdr(thing);
        if(thing)
            fprintf(tgtStream, " ");
    }
    if(withinList)
        fprintf(tgtStream, ")");
    return last;
}

static cell_th *raw_depiction(FILE *targetStream, cell_th *thing, const char *separator) {
    inner_depict(targetStream, thing);
    fprintf(targetStream, "%s", separator);
    return NULL;
}

cell_th *show(cell_th *thing) {
    return raw_depiction(stdout, thing, "");
}

cell_th *depict(cell_th *thing) {
    return raw_depiction(stdout, thing, "\n");
}

cell_th *depict_error(cell_th *errThing) {
    return raw_depiction(stderr, errThing, "\n");
}

const char *debug_lbl(const cell_th *thing) {
    switch(th_kind(thing)) {
        case null_k: return "nil"; 
        case error_k: return "error"; 
        case atom_k: return "atom"; 
        case number_k: return "number"; 
        case string_k: return "string"; 
        case cons_k: return "cons"; 
        case grid_k: return "grid"; 
        case method_k: return "c-vau";
        case routine_k: return "routine"; 
        case procedure_k: return "procedure"; 
        case macro_k: return "macro"; 
        case gen_k: return "gen"; 
        case stream_k: return "stream";
    }
    return "UNKNOWN!";
}

static int inner_dbglst(int depth, const cell_th *thing) {
    int i;
    for(i=0;i<depth;++i) 
        fprintf(stdout, " ");
    fprintf(stdout, "%s @%d@: %s\n", 
           debug_lbl(thing),
           (int)thing,
           sym(thing));
    if(Car(thing)) {
        for(i=0;i<depth;++i) fprintf(stdout, " ");
        inner_dbglst(depth+1, Car(thing));
    } 
    if(Cdr(thing))
        inner_dbglst(depth, Cdr(thing));
    return 0;
}

int debug_list(const cell_th *thing) {
    inner_dbglst(0, thing);
    return 0;
}
