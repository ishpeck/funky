#!/bin/sh
echo '{foo:"shall not see twice"}' | ./funk_machine ./indispensibles/core.srfm | sed -e 's/shall not see twice/~~~/' | grep 'shall not see twice' || exit 0
echo "The last element of the grid should not be dumped outside the grid's body."
exit 1
