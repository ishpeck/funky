#!/bin/sh
echo '(seq 1 100) -> (map print) -> (take 10) ~> print' | ./funk_machine ./indispensibles/core.srfm | grep 11 || exit 0
echo "We should not resolve the printing of 11 if we only take 10"
exit 1
