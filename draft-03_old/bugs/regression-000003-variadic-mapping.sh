#!/bin/sh
echo '(apply print (map + [1 2 3] [4 5 6]))' | ./funk_machine ./indispensibles/core.srfm 2>&1 | grep 'Invalid numeric value: nil' && echo 'Map should not attempt to apply the nil at the end of the list' && exit 1
echo '(resolve (map (def (x) (+ x 3)) (list 1 2 3 4)))' | ./funk_machine ./indispensibles/core.srfm 2>&1 | grep '4 5 6 7 (def' && echo 'Map should not attach the stencil to the end of the returned list' && exit 1
exit 0
