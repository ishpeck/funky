#ifndef _FUNKY_HEADER_FILE_
#define _FUNKY_HEADER_FILE_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "texty.h"
#include "griddy.h"

#include "funky_buffer.h"

#define FUNKY_NEGATIVE 0
#define FUNKY_AFFIRMATIVE 1

#define GC_REGISTER 1
#define GC_SKIPREG 0

#define ANONYMOUS_KW "&anon"
#define UNKNOWN_HANDLER "&unknown-handler"
#define UNKNOWN_NIL "&unknown-nil"
#define UNKNOWN_LIT "&unknown-lit"
#define UNKNOWN_ERR "&unknown-err"

#define ERRMSG_TYPES "Type error"
#define ERRMSG_BADDEF "Cannot construct a procedure with the data given."

typedef enum {
    null_k,
    error_k,
    atom_k,
    number_k,
    string_k,
    cons_k,
    grid_k,
    routine_k,
    method_k,
    procedure_k,
    macro_k,
    gen_k,
    stream_k
} kind_t;

typedef struct {
    kind_t kind;
} cell_th;

#define REPR_TH(x) typedef struct { kind_t kind; char *repr; } x
#define LST_TH(x) typedef struct { kind_t kind; cell_th *CAR; cell_th *CDR; } x

REPR_TH(atom_th);
REPR_TH(number_th);
REPR_TH(string_th);

LST_TH(cons_th);
LST_TH(err_th);
LST_TH(procedure_th);
LST_TH(macro_th);
LST_TH(gen_th);

typedef cell_th *(*c_routine)(cell_th *);
typedef int (*finder_routine)(cell_th *, cell_th *);
typedef int (*gt_ineq_routine)(cell_th *, cell_th *);
typedef int (*monadic_predicate)(const cell_th *);

typedef struct {
    kind_t kind; 
    c_routine routine;
} routine_th;

typedef struct {
    kind_t kind; 
    c_routine routine;
} method_th;

typedef struct {
    kind_t kind; 
    grid_t *data;
} grid_th;

typedef enum {
    stream_broken,
    stream_descriptor,
    stream_on_platter,
    stream_sub_process
} stream_kind;

typedef enum {
    mode_read,
    mode_write,
    mode_append
} stream_mode;

typedef struct {
    kind_t kind;
    stream_kind streamKind;
    stream_mode mode;
    FILE *data;
} stream_th;

#define REPR_BLDR(fnm,typ,knd) cell_th *fnm(const char *label, int shallReg) {  \
    typ *newThing=calloc(1, sizeof(typ));                                        \
    newThing->kind=knd; asprintf(&newThing->repr, "%s", label);                  \
    return shallReg ? reg_thing((cell_th *)newThing) : (cell_th *) newThing;   \
  }

#define REPR_DSTR(fnm,typ) int fnm(cell_th *delMe) {   \
    typ *thing=(typ *)delMe;                            \
    erase_string(thing->repr);                          \
    free(thing);                                        \
    return 0;                                           \
  }

#define LST_BLDR(fnm,typ,knd) cell_th *fnm(cell_th *car, cell_th *cdr, int shallReg) { \
    typ *newThing=calloc(1, sizeof(typ));                               \
    newThing->kind=knd; newThing->CAR=car;                              \
    newThing->CDR=cdr;                                                  \
    return shallReg ? reg_thing((cell_th *)newThing) : (cell_th *) newThing; \
  }

#define LST_DSTR(fnm,typ) int fnm(cell_th *delMe) {    \
    typ *thing=(typ *)delMe;                            \
    thing->CAR=NULL;                                    \
    thing->CDR=NULL;                                    \
    free(thing);                                        \
    return 0;                                           \
  }

int del_atom(cell_th *delMe);
int del_number(cell_th *delMe);
int del_string(cell_th *delMe);

int del_cons(cell_th *delMe);
int del_err(cell_th *delMe);
int del_proc(cell_th *delMe);
int del_mac(cell_th *delMe);
int del_gen(cell_th *delMe);

cell_th *reg_thing(cell_th *thing);

cell_th *Primordial_Atom(const char *label, int shallReg);
cell_th *Primordial_Number(const char *label, int shallReg);
cell_th *Primordial_String(const char *label, int shallReg);
cell_th *Primordial_Cons(cell_th *car, cell_th *cdr, int shallReg);
cell_th *Primordial_Err(cell_th *messages, int shallReg);
cell_th *Primordial_Proc(cell_th *car, cell_th *cdr, int shallReg);
cell_th *Primordial_Mac(cell_th *car, cell_th *cdr, int shallReg);
cell_th *Primordial_Gen(cell_th *car, cell_th *cdr, int shallReg);
cell_th *Primordial_Routine(c_routine theFunction, int shallReg);
cell_th *Primordial_CVau(c_routine theFunction, int shallReg);
cell_th *Primordial_Grid(int shallReg);

cell_th *Atom(const char *label);
cell_th *Number(const char *label);
cell_th *String(const char *label);
cell_th *Cons(cell_th *car, cell_th *cdr);
cell_th *Err(cell_th *messages);
cell_th *Proc(cell_th *car, cell_th *cdr);
cell_th *Mac(cell_th *car, cell_th *cdr);
cell_th *Gen(cell_th *car, cell_th *cdr);

cell_th *Routine(c_routine theFunction);
cell_th *CVau(c_routine theFunction);
cell_th *Grid(void);

cell_th *F_Stream(const char *path, stream_mode mode);
cell_th *D_Stream(int fdnum, stream_mode mode);

int del_routine(cell_th *thing);
int del_grid(cell_th *grid);

kind_t th_kind(const cell_th *thing);
int is_atom(const cell_th *thing);
int is_number(const cell_th *thing);
int is_gen(const cell_th *thing);
int is_list(const cell_th *thing);
int is_lambda(const cell_th *thing);
int is_string(const cell_th *thing);
int is_functor(const cell_th *thing);
int is_grid(const cell_th *thing);
int is_error(const cell_th *thing);
int has_repr(const cell_th *thing);
const char *sym(const cell_th *thing);
cell_th *Car(const cell_th *thing);
cell_th *Cdr(const cell_th *thing);
c_routine call_rt(cell_th *thing);
int del_thing(cell_th *thing);
cell_th *set_cdr(cell_th *cell, cell_th *cdr);
cell_th *set_car(cell_th *cell, cell_th *car);
cell_th *Set(cell_th *grid, const char *kw, cell_th *val);
cell_th *Get(cell_th *grid, const char *kw);
int Has(cell_th *grid, const char *kw);
cell_th *last_el(cell_th *thing);
cell_th *append(cell_th *left, cell_th *right);
cell_th *Keys(const cell_th *grid);
cell_th *Vals(const cell_th *grid);

stream_kind stream_type(cell_th *thing);
FILE *stream_source(cell_th *thing);
int set_stream_kind(cell_th *thing, stream_kind knd);

cell_th *insert(cell_th *left, cell_th *right);
cell_th *accumulate(cell_th *thing);
cell_th *duplicate(cell_th *thing);
unsigned int count_env_levels(void);
cell_th *reg_to_parent(cell_th *thing);

cell_th *rootEnvironment;
cell_th *rootBacros;
cell_th *env;
cell_th *unknownSymbolError;

int establish_root_environment(void);
cell_th *scope_containing(const char *label);
cell_th *lookup_txt(const char *label);
cell_th *lookup_sym(const cell_th *label);
int new_env(void);
cell_th *push_env(cell_th *newScope);
int pop_env(void);
cell_th *env_set(const char *label, cell_th *thing);
void wipe_env(void);

cell_th *eval(cell_th *expr);
cell_th *apply(cell_th *lambdaAndArgs);

cell_th *dirty_sum(cell_th *args); 
cell_th *dirty_sub(cell_th *args);
cell_th *funky_if(cell_th *args);
cell_th *funky_set(cell_th *args);
cell_th *funky_print(cell_th *args);
cell_th *funky_macro(cell_th *args);
cell_th *funky_quote(cell_th *args);
cell_th *funky_head(cell_th *args);
cell_th *funky_rest(cell_th *args);
cell_th *funky_list(cell_th *args);
cell_th *funky_def(cell_th *args);
cell_th *funky_last(cell_th *args);
cell_th *funky_err(cell_th *args);
cell_th *funky_dump(cell_th *args);
cell_th *funky_greater_than(cell_th *args);
cell_th *funky_less_than(cell_th *args);
cell_th *funky_equivalent(cell_th *args);
cell_th *funky_not_operator(cell_th *args);
cell_th *funky_evaluator(cell_th *args);
cell_th *funky_grid(cell_th *args);
cell_th *funky_grid_get(cell_th *args);
cell_th *funky_truthy(cell_th *args);
cell_th *funky_nilly(cell_th *args);
cell_th *funky_callable(cell_th *args);
cell_th *funky_is_atom(cell_th *args);
cell_th *funky_is_gen(cell_th *args);
cell_th *funky_is_str(cell_th *args);
cell_th *funky_length(cell_th *args);
cell_th *funky_gen(cell_th *args);
cell_th *funky_cons(cell_th *args);
cell_th *funky_append(cell_th *args);
cell_th *funky_is_error(cell_th *args);
cell_th *funky_is_grid(cell_th *args);
cell_th *funky_make_txt(cell_th *args);
cell_th *funky_type_symbol(cell_th *args);
cell_th *funky_resolve_gen(cell_th *thisGen);
cell_th *funky_resolve_entire_gen(cell_th *args);
cell_th *funky_list_contains(cell_th *args);
cell_th *funky_rand_int(cell_th *args);

cell_th *funky_file_reader(cell_th *args);
cell_th *funky_get_byte(cell_th *args);

cell_th *show(cell_th *thing);
cell_th *depict(cell_th *thing);
cell_th *depict_error(cell_th *errThing);
const char *debug_lbl(const cell_th *thing);
int debug_list(const cell_th *thing);

int get_character(FILE *src);
cell_th *read_exprs(FILE *src);
cell_th *expand_bacros(cell_th *head);

#endif

