#include "funky.h"

static long text_to_long(cell_th *numsrc) {
    char *lastValidChar;
    long output=strtol(sym(numsrc), &lastValidChar, 10);
    if(*lastValidChar!='\0')
        fprintf(stderr, "Invalid numeric value: %s\n", sym(numsrc));
    return output;
}

cell_th *dirty_sum(cell_th *args) {
    long num=0;
    char *outty;
    cell_th *output;
    if(!args)
        return Number("0");
    while(args) {
        num+=text_to_long(Car(args));
        args=Cdr(args);
    }
    asprintf(&outty, "%ld", num);
    output=Number(outty);
    erase_string(outty);
    return output;
}

cell_th *dirty_sub(cell_th *args) {
    long num=0;
    char *outty;
    cell_th *output;
    if(!args)
        return Number("0");
    if(Cdr(args)) {
        num=text_to_long(Car(args));
        args=Cdr(args);
    }
    while(args) {
        num-=text_to_long(Car(args));
        args=Cdr(args);
    }
    asprintf(&outty, "%ld", num);
    output=Number(outty);
    erase_string(outty);
    return output;
}

static cell_th *inner_funky_if(cell_th *predicate,
                                cell_th *positive, 
                                cell_th *negative) {
    return eval(eval(predicate) ? positive : negative);
}

cell_th *funky_if(cell_th *args) {
    return inner_funky_if(Car(args),
                          Car(Cdr(args)),
                          Car(Cdr(Cdr(args))));
}

static cell_th *inner_funky_set(cell_th *label,
                                 cell_th *expr) {
    return env_set(sym(label), expr);
}

cell_th *funky_set(cell_th *args) {
    return inner_funky_set(Car(args),
                           Car(Cdr(args)));
}

cell_th *funky_print(cell_th *args) {
    cell_th *prev;
    while(args) {
        prev=Car(args);
        if(is_string(Car(args)))
            printf("%s", sym(Car(args)));
        else
            show(Car(args));
        args=Cdr(args);
        if(args)
            printf(" ");
    }
    printf("\n");
    return prev;
}

static int funky_things_are_equivalent(cell_th *left, cell_th *right) {
    return th_kind(left)==th_kind(right) && streq(sym(left), sym(right));
}

#define TH_CMP(cmpr) recursive_lists_are_equivalent(cmpr(left), cmpr(right))

static int recursive_lists_are_equivalent(cell_th *left, cell_th *right) {
    if(th_kind(left)==string_k && th_kind(right)==string_k)
        return streq(sym(left), sym(right));
    if(!funky_things_are_equivalent(left, right))
        return 0;
    return !left || (TH_CMP(Car) && TH_CMP(Cdr));
}

static int list_contains_thing(cell_th *haystack, cell_th *needle) {
    while(haystack) {
        if(funky_things_are_equivalent(Car(haystack), needle))
            return 1;
        haystack=Cdr(haystack);
    }
    return 0;
}

static int grid_contains_thing(cell_th *haystack, cell_th *needle) {
    return Has(haystack, sym(needle));
}

static cell_th *contains_any_of_needles(finder_routine fndr, 
                                         cell_th *haystack, 
                                         cell_th *needles) {
    while(needles) {
        if(fndr(haystack, Car(needles)))
            return lookup_txt("true");
        needles=Cdr(needles);
    }

    return NULL;
}

cell_th *funky_list_contains(cell_th *args) {
    return contains_any_of_needles( ((is_grid(Car(args))) ? 
                                     grid_contains_thing :
                                     list_contains_thing) ,
                                    Car(args),
                                    Cdr(args) );
}

cell_th *funky_equivalent(cell_th *args) {
    cell_th *cur=args;
    while(cur && Cdr(cur)) {
        if(!recursive_lists_are_equivalent(Car(cur), Car(Cdr(cur))))
            return NULL;
        cur=Cdr(cur);
    }
    return lookup_txt("true");
}

static cell_th *substitute_value(cell_th *symb, cell_th *val) {
    if(val==unknownSymbolError) 
        return symb;
    switch(th_kind(val)) {
        case procedure_k: case routine_k: case method_k:
        case macro_k: case gen_k: case null_k: 
            return symb;
        default: 
            return val;
    }
}

static cell_th *resolve_names_for_this_scope(cell_th *locals, 
                                              cell_th *body) {
    cell_th *cur=body;
    while(cur) {
        if(is_list(Car(cur)))
            set_car(cur, resolve_names_for_this_scope(locals, Car(cur)));
        else if(is_atom(Car(cur)) && 
                !list_contains_thing(locals, Car(cur)) &&
                !streq(sym(Car(cur)), "&args"))
            set_car(cur, substitute_value(Car(cur), lookup_sym(Car(cur))));
        cur=Cdr(cur);
    }
    return body;
}

static cell_th *build_macro(cell_th *args) {
    return Mac(Car(args), 
               resolve_names_for_this_scope(Car(args), 
                                            Cdr(args)));
}

cell_th *funky_macro(cell_th *args) {
    if(has_repr(Car(args))) {
        return env_set(sym(Car(args)), build_macro(Cdr(args)));
    }
    return build_macro(args);
}

cell_th *funky_quote(cell_th *args) {
    return Car(args);
}

cell_th *funky_head(cell_th *args) {
    return Car(Car(args));
}

cell_th *funky_rest(cell_th *args) {
    return Cdr(Car(args));
}

cell_th *funky_list(cell_th *args) {
    return args;
}

static cell_th *define_procedure(cell_th *args) {
    return Proc(Car(args), resolve_names_for_this_scope(Car(args), 
                                                        Cdr(args)));
}

cell_th *funky_def(cell_th *args) {
    switch(th_kind(Car(args))) {
        case atom_k:
            return env_set(sym(Car(args)),
                           define_procedure(Cdr(args)));
        case cons_k: case null_k:
            return define_procedure(args);
        default:
            return Err(Cons(Atom(ERRMSG_TYPES),
                            Cons(Atom(ERRMSG_BADDEF), NULL)));
    }
}

cell_th *funky_last(cell_th *args) {
    return Car(last_el(Car(args)));
}

cell_th *funky_err(cell_th *args) {
    return Err(args);
}

cell_th *funky_dump(cell_th *args) {
    while(args) {
        debug_list(Car(args));
        args=Cdr(args);
    }
    return args;
}

static int funky_compare_numbers(cell_th *left, cell_th *right) {
    long lnum=text_to_long(left);
    long rnum=text_to_long(right);
    return lnum>rnum;
}

static int funky_compare_strings(const char *left, const char *right) {
    int i=0;
    int llen=strlen(left);
    int rlen=strlen(right);
    int max=(llen>rlen) ? rlen : llen;
    for(i=0;i<max;++i)
        if(left[i]<right[i])
            return 0;
    return 1;
}

static int lft_greater_than_rit(cell_th *lft, cell_th *rit) {
    if(is_number(lft)) {
        if(is_number(rit))
            return funky_compare_numbers(lft, rit);
        else if(!rit)
            return FUNKY_AFFIRMATIVE;
    }
    if(is_string(lft) && is_string(rit))
        return funky_compare_strings(sym(lft), sym(rit));
    fprintf(stderr, "Don't know how to compare the types given: %s, %s\n", debug_lbl(lft), debug_lbl(rit));
    return 0;
}

static int rit_greater_than_lft(cell_th *lft, cell_th *rit) {
    return lft_greater_than_rit(rit, lft);
}

static cell_th *raw_funky_comparison(gt_ineq_routine cmp,
                                      cell_th *cur) {
    while(cur && Cdr(cur)) {
        if(!cmp(Car(cur), Car(Cdr(cur))))
            return NULL;
        cur=Cdr(cur);
    }
    return lookup_txt("true");
}

cell_th *funky_greater_than(cell_th *args) {
    return raw_funky_comparison(lft_greater_than_rit, args);
}

cell_th *funky_less_than(cell_th *args) {
    return raw_funky_comparison(rit_greater_than_lft, args);
}

cell_th *funky_not_operator(cell_th *args) {
    if(!Car(args))
        return lookup_txt("true");
    return NULL;
}

static cell_th *horrendous_recursion_trap_eval(cell_th *args) {
    return eval(Car(args));
}

cell_th *funky_evaluator(cell_th *args) {
    return horrendous_recursion_trap_eval(args);
}

static int set_grid_pair_data(cell_th *grid,
                              cell_th *key,
                              cell_th *val) {
    if(!key)
        return 0;
    Set(grid, sym(key), val);
    return 1;
}

static int set_grid_pair(cell_th *grid, cell_th *pair) {
    return pair && set_grid_pair_data(grid, Car(pair), Cdr(pair));
}

static cell_th *resolve_gen_data(cell_th *gendat) {
    return apply(Cdr(gendat));
}

cell_th *funky_grid(cell_th *args) {
    cell_th *grid=Grid();
    while(set_grid_pair(grid, Car(args)))
        args=(th_kind(args)==gen_k) ? resolve_gen_data(args) : Cdr(args);
    return grid;
}

static cell_th *funky_grid_getter(cell_th *grid,
                                   cell_th *keyword) {
    return Get(grid, sym(keyword));
}

cell_th *funky_grid_get(cell_th *args) {
    return funky_grid_getter(Car(args),
                             Car(Cdr(args)));
}

static int this_cell_is_truthy(cell_th *cell) {
    switch(th_kind(cell)) {
        case grid_k:
            return Keys(cell)!=NULL;
        case error_k: case null_k:
            return 0;
        case cons_k: case macro_k: 
        case gen_k: case procedure_k:
            return Car(cell)!=NULL || Cdr(cell)!=NULL;
        case atom_k:
            return !streq(sym(cell), "nil");
        case number_k: case string_k:
            return 1;
        case routine_k: case method_k:
            return call_rt(cell)!=NULL;
        case stream_k:
            return stream_type(cell)!=stream_broken;
    }
}

cell_th *funky_truthy(cell_th *args) {
    return this_cell_is_truthy(Car(args)) ? lookup_txt("true") : NULL;
}

cell_th *funky_nilly(cell_th *args) {
    return Car(args) ? NULL : lookup_txt("true");
}

static cell_th *funky_type_id_sweep(monadic_predicate pred,
                                     cell_th *candidates) {
    while(candidates) {
        if(!pred(Car(candidates)))
            return NULL;
        candidates=Cdr(candidates);
    }
    return lookup_txt("true");
}

cell_th *funky_callable(cell_th *args) {
    return funky_type_id_sweep(is_lambda, args);
}

cell_th *funky_is_atom(cell_th *args) {
    return funky_type_id_sweep(is_atom, args);
}

cell_th *funky_is_gen(cell_th *args) {
    return funky_type_id_sweep(is_gen, args);
}

cell_th *funky_is_str(cell_th *args) {
    return funky_type_id_sweep(is_string, args);
}

static cell_th *inner_funky_length(cell_th *args) {
    unsigned long len=0;
    char *num;
    cell_th *outcome;
    while(args && (Cdr(args) || Car(args))) {
        ++len;
        args=Cdr(args);
    }
    asprintf(&num, "%ld", len);
    outcome=Number(num);
    erase_string(num);
    return outcome;
}

cell_th *funky_length(cell_th *args) {
    return inner_funky_length(Car(args));
}

cell_th *funky_gen(cell_th *args) {
    if(!Car(args))
        return Err(Cons(String("Incomplete Gen construction"), NULL));
    return Gen(Car(args), Cdr(args));
}

cell_th *funky_cons(cell_th *args) {
    return Cons(Car(args), Car(Cdr(args)));
}

cell_th *funky_append(cell_th *args) {
    cell_th *output=duplicate(Car(args));
    while(Cdr(args)) {
        output=append(output, Car(Cdr(args)));
        args=Cdr(args);
    }
    return output;
}

cell_th *funky_is_error(cell_th *args) {
    return funky_type_id_sweep(is_error, args);
}

cell_th *funky_is_grid(cell_th *args) {
    return funky_type_id_sweep(is_grid, args);
}

static cell_th *inner_mktxt(text_buffer *tb, cell_th *args) {
    while(args) {
        const char *txt=sym(Car(args));
        long i;
        long m=strlen(txt);
        for(i=0;i<m;++i) {
            tb_append(tb, txt[i]);
        }
        args=Cdr(args);
    }
    return String(tb->txt);
}

cell_th *funky_make_txt(cell_th *args) {
    text_buffer *tb=tb_new();
    cell_th *ret=inner_mktxt(tb, args);
    tb_wipe(tb);
    return ret;
}

static cell_th *identifyTypes(cell_th *args, cell_th *cur) {
    while(args) {
        cur=set_cdr(cur, Cons(String(debug_lbl(Car(args))), NULL));
        args=Cdr(args);
    }
    return cur;
}

cell_th *funky_type_symbol(cell_th *args) {
    cell_th *head=NULL;
    if(args) {
        head=Cons(String(debug_lbl(Car(args))), NULL);
        identifyTypes(Cdr(args), head);
    }
    return head;
}

cell_th *funky_resolve_gen(cell_th *thisGen) {
    return (is_gen(Car(thisGen))) ? resolve_gen_data(Car(thisGen)) : thisGen;
}

static cell_th *random_number(long base, 
                               long offset, 
                               long upper) {
    char *nm;
    cell_th *num;
    asprintf(&nm, "%ld", (base%(upper-offset+1))+offset);
    num=Number(nm);
    erase_string(nm);
    return num;
}

static cell_th *random_integer(cell_th *begin, 
                                cell_th *finish) {
    if(!begin || !finish)
        return Err(Cons(String("Random number sequence requires a lower bound, upper bound, and the quantity of random numbers to generate."), NULL));
    return random_number((long)rand(),
                         text_to_long(begin),
                         text_to_long(finish));
}

cell_th *funky_rand_int(cell_th *args) {
    return random_integer(Car(args),
                          Car(Cdr(args)));
}

cell_th *funky_file_reader(cell_th *args) {
    return F_Stream(sym(Car(args)), mode_read);
}

static cell_th *get_byte_from_stream(cell_th *stream) {
    int gotten;
    char *inpt;
    cell_th *byte;
    if(!th_kind(stream)==stream_k)
        return Err(Cons(String("Can't get byte from non-stream."), NULL));
    if(stream_type(stream)==stream_broken)
        return Err(Cons(String("Can't get byte from broken stream."), NULL));
    if(!stream_source(stream))
        return Err(Cons(String("Stream appears to be broken."), NULL));
    if((gotten=getc(stream_source(stream)))==EOF)
        return NULL;
    asprintf(&inpt, "%c", (char)gotten);
    byte=String(inpt);
    erase_string(inpt);
    return byte;
}

cell_th *funky_get_byte(cell_th *args) {
    return get_byte_from_stream(Car(args));
}
