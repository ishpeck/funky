# Funky Spec

Specifications for Funky.

# Introduction

Funky is a process interop environment designed to glue autonomous
programs and services together with a thin computer interface language
similar to the way UNIX Shell operates.

Language fetaures include lazy evaluation, native support for JSON
objects, implicit partial application of functions, fexprs and the
ability to redefine the language's syntax on the fly to make it easier
to plug programs together.

# Tokens and Parsing

Here we describe the basic parsing and tokenization of the Funky
language.

## Case Sensitivity

Unlike other Lisps, Funky is case sensitive.  Thus `foo` and `Foo` are
different symbols.

## Whitespace

Whitespace characters separate symbol names and are therefore called
"symbol boundary" characters.  Where multiple consecutive whitespace
characters appear, there is no significance beyond the first which
denotes the end of a symbol.

The following are considered to be whitespace:

```
| NAME            | CHARACTER | ASCII CODE | Escaped  
|-----------------+-----------+------------+--------  
| Tab             |           |          9 | \t       
| Newline         |           |         10 | \n       
| Carriage Return |           |         13 | \r       
| Space           |           |         32 |          
| Comma           | ,         |         44 |          
```

**Note:** Commas are whitespace!  "Why" is explained in the
`Braces/Grids` section.

## Dot getter

When not embedded in a numeric atom (see Numbers below), the period
(`.` or ASCII 46) denotes symbol boundary and is interchangeable with
calling `get` on the symbols surrounding it.

The following

```
   foo.bar
   foo . bar
   foo     . bar
   foo,.,bar
```

Are all interchangeable with...

```
   (get foo bar)
```

## Parenthesis

Parenthesis denote the beginning and end of a Symbolic Expression
(sexpr) as well as symbol boundary.

## Jigging

The colon (:) denotes symbol boundary and creates a `pair` ("dotted
cons" in other Lisps) consisting of the symbols surrounding it.

Thus the funky expression...

```
   x:y
```

... is equivalent to the following Scheme/Common Lisp expression:

```
  (x . y)
```

The dotted cons does not exist in Funky as the `.` is reserved for the
getter.  Note that parenthesis do not surround this "jigged pair."

Jigged pairs are also interchangeable with the pair sexpr:

```
   (pair x y)
```

## Special Enclosures

In addition to parenthesis, special enclosing characters are used to
signify special sexprs.

### Brackets/Lists

Square bracket characters [ and ] denote the beginning and end of a
list respectively.  Each element of the list is surrounded by one or
more whitespace character.

Valid list forms may include:

```
   [1 2 3 4]

   ["foo","bar","bas","quux"]

   [a, s, d, f]

   [1
    2
    3,4]
```

Any expression in the form of `[...]` is interchangeable with a sexpr
in the form of `(list ...)`

### Braces/Grids

Curly brace characters `{` and `}` denote the beignning and end of a
`grid` respectively.  A `grid` is a list whose elements represent an
"association" (jigged pairs) where the associative pair's first symbol
is a "key" and the second the "value" associated with it.

The following expressions...

```
   {(pair "a" "alpha") (pair "b" "bravo")}

   {"a":"alpha" "b":"bravo"}

   {"a":"alpha", "b":"bravo"}
```

Are interchangeable with...

```
   (grid (pair "a" "alpha") (pair "b" "bravo"))
```

Valid forms of JSON (RFC 7159) must be readable as valid grid
expressions in the Funky interpreter.

Grid data may be constructed in any valid form of Funky notation (such
as those defined above) but must always be depicted in a format
consumable by JSON parsers.

## Numbers

Numbers are any symbol that meet the following criteria:

 - Begins with a numeric character (0-9) OR begins with a dash (`-`)
   followed by at least one numeric character
 - Any number of additional numeric characters
 - At most one period (`.`)
 - At most one `E` character which must be preceded by numeric
   characters and may be followed by either numeric characters or a
   dash plus at least one numeric character.

Valid numbers include:

```
   0
   18374
   -24
   3.141596
   1.13413E10
```

## Text

Any series of characters (including whitespace) surrounded by the same
text-delimiter constitute a text atom (string).  String deliniators
are any matching pair of single quote, double quote or backticks.

```
   "this is text"
   'this is also text
   'this one has "quote" characters in it'
   "That's all, folks!"
   `"That's pretty scary!"`
```

Each line above contains a single complete piece of text.  Text may
also consist of multiple lines.

```
   "this
   is
   a
   multi-line
   string"
```

The newlines will be taken literally in such a string.

## Comments

Unless encapsulated in quotation marks, all characters following the
first pound sign (`#`) until the end of the line are ignored by the
interpreter as "comments."

## Reserved Prefix: Ampersand

Symbols beginning with `&` are reserved for the interpreter's use and
can not be declared by the Funky programmer.

Funky spec does not define which `&-primitives` may exist.  This is up
to the implementation to decide.  The spec does specify that any
attempt by the programmer to declare procedures or variables beginning
with the `&` character should result in an error.

```
   (def &foo (x y) (+ x y ))
```

Results in:

```
   (error "User-defined procedures may not begin with &" "&foo")
```

```
   (def bar (&x y) (+ &x y))
```

Results in:

```
   (error "User-defined operands may not begin with &" "&x")
```

The intent is for any symbol not so prefixed to be reassignable in the
current scope.  This is generally considered "bad" but sometimes you
want to be able to take some things for granted.  You're prototyping,
after all.  It may therefore be the case that... `nil` points to
`&nil` by default and you could ruin the whole world by going...

```
(set! nil true)
```

...you filthy gremlin!

# Atomic Primitives

In addition to numbers, we have the following atomic symbols.

## nil

An empty expresion.  Used to represent falseyness.

## true

`true` is interchangeable with the expression `(not nil)`

## false

`false` is a synonym for `nil`

# Standard Procedures and Macros

The following procedures must be defined and universally available in
the Funky language.

## Arithmetic operators

### + (variadic)

Evaluates to the sum of the given numeric arguments.  Requires numeric
values.

### - (variadic)

Evaluates to the opposite of the  first argument if no other arguments
are given.  Otherwise subtracts the second argument from the first and
each subsequent argument from the difference of the previous two until
and evaluates down to the last difference.  Requires numeric values.

### * (variadic)

Evaluates to the product of the given arguments.  Requires numeric
types.

### / (variadic)

Evaluates to the  inverse of the first argument if  no other arguments
are given.  Divides  the first argument by the second  and divides its
quotient by the subsequent argument until no more arguments are found.
Requires numeric types.

Evaluates to an error if the only argument given is zero or if any
arguments after the first are zero.

### ^ (variadic)

Raises each argument given to the power of the next.  Requires numeric
types.

## Boolean logic operators

### and (variadic)

Evaluates to the last argument if all arguments given are truthy or to
`false` if any is falsey.

### or (variadic)

Evaluates to the first truthy argument given or `false` if none are
truthy.

### not (monadic)

Evaluates to `false` if argument is truthy or to `true` otherwise.

### = (variadic)

Evaluates to `true` if all arguments have the same value or `false`
otherwise.

### > (variadic)

Evaluates to `true` if each argument is less than the previous.
Otherwise, evaluates to `false`.

### < (variadic)

Evaluates to `true` if each argument is greater than the previous.
Otherwise, evaluates to `true`.

### >= (variadic)

Evaluates to `true` if each argument is less than or equal to the
previous.  Otherwise, evaluates to `false`.

### <= (variadic)

Evaluates to `true` if each argument is greater than or equal to the
previous.  Otherwise, evaluates to `false`.

## Basic Operators

### begin (variadic)

Evaluates to the last expression given after evaluating all previous
expressions in order.

### cond (variadic)

The conditional macro expression similar to those in other Lisps.

Receives a series of pairs which contain predicates as their first
elements and associated expressions for the second elements.
Evaluates each predicate in sequence.  Upon the first predicate that
evaluates as truthy, its associated expression is evaluated and no
further predicates will be evaluated.

```
   (cond
      (= (type foo) "number"):(print! "foo is a number")
      (= (type foo) "text"):(print! "foo is text")
      true:(print! "Not sure what foo is")
   )
```

### def (at least triadic)

Evaluates to  a lambda  expression that  may be  named in  the current
environment if  the first  argument is an  atom.  The  second argument
must be an operand sexpr if the first is an atom.  Otherwise the first
argument must be  an operand sexpr.  Each  subsequent expression given
represents the body of the procedure.

### embed! (monadic)

Evaluates the contents of a file within the current file's context.
Similar to a `#include` directive in `C/C++`.

### error (variadic)

Creates  an   error  expression   containing  the   given  informative
expressions to  help inform the user  of why the error  was generated.
Errors should break safe evaluation chains.

### gen (dyadic)

Generator, or "generated  list" is evaluated to itself  and is treated
as if  it were a  `list` in almost  all other cases.   Generators help
produce lazy lists where the contents are not known until requested as
by the `first` procedure or something similar.

### if (at least dyadic)

An `if` macro expression as found in other languages.

```
   (if (= (% x 2) 0) "even" "odd")
```

The above is interchangeable with:

```
   (cond
      (= (% x 2) 0):"even"
      true:"odd"
   )
```

And the following expression...

```
   (if (= nm "Bob") (print! "Hello, Bob!"))
```

... is interchangeable with...

```
   (cond 
      (= nm "Bob"):(print! "Hello, Bob!")
      true:nil
   )
```

### import! (at least dyadic)

Evaluates the contents of a file specified in the second argument and
stores the evaluated context as a `grid` under the variable name of
the first argument.

### let (variadic)

Evaluates to its last expression within  a new context defined by each
previous expression  (called "contextuals").  The context  defines the
`head` of each contextual as the `next` of the same.

The following evaluates to `7`:

```
(let x:3 y:4 (+ x y))
```

Whereas a new context is created where the symbol `x` is given the
value `3` and the symbol `y` has `4`.

### quote (variadic)

Skips evaluation and evaluates to itself.

```
(quote foo bar bas)
```

Evaluates to:

```
(quote foo bar bas)
```

### set! (dyadic)

Assigns a  global variable.  The first  argument is the new  symbol of
the global variable and the second argument is evaluated to become the
value of that global.

```
(set! x 3)
```

If you want local variables, use `let` or a procedure scope instead.

### seq (dyadic or triadic)

Evaluates to a generated list starting with the value of the first
argument and ending with the value of the second argument.  If second
is greater than first, increments (`+1`) per element.  If second is
less than the first, decrements (`-1`).

If third argument is given, step is multiplied by the third argument's
value to specify the range.

### type (monadic)

Evaluates to a text atom identifying the type of the given argument.

```
(type 123)
"number"
(type "foo")
"text"
(type map)
"procedure"
```

## IO Operators

### print! (variadic)

Writes the string depictions of  the given expressions to the standard
output bytestream.

### run! (variadic)

Prepares to start a subprocess with the command given in the arguments
where at  least one argument  is given.   Arguments may be  strings or
symbols that make the name of an  executable found in the PATH as well
as the options passed into that program.

Evaluates to an error if the  program cannot be run.  Evaluates to nil
if  no  arguments  are  provided.  Evaluates  to  a  spigot  procedure
procedure that receives an input source for the program and spawns the
specified process once called.

#### TODO Multi-IO considerations

How shall we make subprocesses connect to multiple IO streams?

UNIX shell has:

```
    foo 4</tmp/blah 5>/tmp/gurg
```

To set program foo's file descriptor 4 to read from /tmp/blah and file
descriptor 5 to  write to the /tmp/gurg file.  What  is the proper way
to do this in Funky?

## Higher Order Procedures

### map (dyadic)

First argument must be a procedure and second argument a sequence.
Applies procedure over each element in the sequence and evaluates to a
list of the return values of each.

```
(map (def (x) (+ x 3)) (list 1 2 3))
```

Evaluates down to:

```
(list 4 5 6)
```

### fold (dyadic or triadic)

First argument is a procedure and second argument is a sequence. The
following code ...

```
(fold + (list 1 2 3 4))
```

... is identical to this code...

```
(+ (+ (+ 1 2) 3) 4)
```

And the triadic form as follows...

```
(fold + (list 3 4 5 6) 2)
```

... is identical to...

```
(fold + (pair 2 (list 3 4 5 6)))
```

### filt (dyadic)

First argument is a procedure and second argument is a sequence.
Passes each element of sequence into given procedure.  For each
element which returns truthy out of the procedure, return that
element, discard all those that evaluate to falsy.

```
(def even? (x) (= (% x 2) 0))
(filt even? (list 1 2 3 4 5 6 7))
```

Evaluates down to...

```
(list 2 4 6)
```

# Notation Convention

Naming of procedures and variables in the Funky language follow a
standard intended to help denote certain properties found in the
symbols being used.

## Bang

Imperative  statements  or procedures  with  side  effects beyond  the
current environment end with an exclamation mark ("bang").

Examples: 

```
  (set! foo 44)

  (run! grep -v donotwant)
```

The bang warns  the user that the consequences of  the given procedure
extend beyond the value that the expression evaluates to.

## Protected Symbols

Symbols beginning with ampersands are considered "protected" in all
contexts and cannot be assigned new values.  Here we define the most
fundamental parts of the language that must remain inviolate if the
rest of the language is to work as designed.

The following evaluates to an `err`:

```
(set! &foo 4)
```

