#!/bin/sh
find ./ -iname '*.c' -exec cat {} \; | grep '\/\*' || exit 0
echo 'This is an anticommentist project.  See http://www.anticommentist.org/ for more info.'
echo 'Please remove all comments from the source code.'
exit 1