#ifndef _LINUX_COMPATIBILITY_HEADER_
#define _LINUX_COMPATIBILITY_HEADER_
#include <string.h>

size_t strlcpy(char *, const char *, size_t);

#endif
