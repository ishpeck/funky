#include "linux_sucks.h"

size_t strlcpy(char *dst, const char *src, size_t size) {
    strncpy(dst, src, size);
    return size;
}
