#include "funky_core.h"

static int fail(const char *msg) {
    fprintf(stderr, "%s\n", msg);
    return 1;
}

static int do_file(FILE *init, FILE *src) {
    if(!src)
        return fail("Invalid source file!");
    run_file(init, src);
    return 0;
}

static int do_repl(FILE *init, FILE *src, const char *prompt) {
    if(!src)
        return fail("Invalid source file!");
    if(src == stdin)
        printf("### Funky - SRFM implementation a0\n");
    repl_main(init, src, prompt);
    return 0;
}

static int do_distilling(FILE *src) {
    if(!src)
        return fail("Invalid source file!");
    comprehend_file(src);
    return 0;
}

static FILE *init_file(void) {
    char *fpath = getenv("FUNKY_INIT_PATH");
    if(!fpath || *fpath=='\0')
        fpath = "/usr/local/funky/init.funk";
    return fopen(fpath, "r");
}

int main(int argc, char **argv) {
    char *targetFilePath;
    FILE *initFile;
    if(argc < 1)
        return fail("First argument must be an initializer file");

    if(argc > 1 && strcmp(argv[1], "-c")==0)
        return do_distilling((argc>2)?(fopen(argv[2], "r")):stdin);

    initFile = init_file();
    if(!initFile)
        return fail("FATAL: Failed to load init.funk!");

    if(argc>2 && strcmp(argv[1], "-f")==0)
        return do_repl(initFile, fopen(argv[2], "r"), NULL);

    if(argc>1)
        return do_file(initFile, fopen(argv[1],"r"));

    return do_repl(initFile, stdin, "#>");
}
