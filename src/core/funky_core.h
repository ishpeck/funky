#ifndef _FUNKY_CORE_HEADER_
#define _FUNKY_CORE_HEADER_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <gc.h>

#include "portability.h"

typedef enum {
    atom_k,
    num_k,
    text_k,
    mem_k,
    cons_k,
    gen_k,
} kind_t;

typedef struct {
    kind_t kind;
} cell_t;

typedef struct {
    kind_t kind;
    size_t max;
    void *data;
} atom_t;

typedef struct {
    kind_t kind;
    long numerator;
    long denominator;
} num_t;

typedef struct {
    kind_t kind;
    cell_t *car;
    cell_t *cdr;
} pair_t;

typedef struct {
    size_t size;
    unsigned int len;
    char *content;
    int isNumeric;
} buffer_t;

typedef cell_t*(*funky_fn)(cell_t *,cell_t*);

typedef struct {
    char *name;
    funky_fn fn;
} fundamental_t;

buffer_t *spawn_buffer(size_t);
int push_char(buffer_t *, const int);
void del_chars(buffer_t *, const unsigned int);
void del_char(buffer_t *);
void clear_buffer(buffer_t *);

atom_t *spawn_atom(const char *, const kind_t);
num_t *spawn_number(const char *);
pair_t *spawn_cons(cell_t *, cell_t *, const kind_t);
void destroy(cell_t *);

cell_t *car(const cell_t *);
cell_t *cdr(const cell_t *);

static void depict_this(FILE *, const cell_t *);
void depict(FILE *, const cell_t *);

cell_t *read_funk_from_file(FILE *);

cell_t *comprehend_syntax(cell_t *);
cell_t *eval(cell_t *, cell_t*);

void delete_buffer(buffer_t *);
void comprehend_file(FILE *);
void repl_main(FILE *, FILE *, const char *);
void run_file(FILE *, FILE *);

#define spawn_symbol(x) ((cell_t *)spawn_atom((x), atom_k))
#define spawn_num(x) ((cell_t *)spawn_number(x))
#define spawn_text(x) ((cell_t *)spawn_atom((x), text_k))
#define spawn_mem(x) ((cell_t *)spawn_atom((x), mem_k))
#define spawn_gen(x,y) ((cell_t *)spawn_cons((x), (y), gen_k))
#define spawn_pair(x,y) ((cell_t *)spawn_cons((x), (y), cons_k))
#define spawn_doublet(x,y) (spawn_pair((x),spawn_pair((y),NULL)))
#define spawn_triplet(x,y,z) (spawn_pair((x),spawn_pair((y),spawn_pair(z, NULL))))
#define spawn_err(x) (spawn_doublet(spawn_symbol("error"),(spawn_text(x))))
#define spawn_errr(x,y) (spawn_triplet(spawn_symbol("error"),(spawn_text(x)),(y)))
#define print(x) depict(stdout, (x))

#define get_kind_of(x) ((x!=NULL)?((x)->kind):(atom_k))
#define is_cons(x) ((x!=NULL)&&(((x)->kind)==cons_k))
#define is_dotted_cons(x) ((is_cons((x)))&&((!cdr(x))||!is_cons(cdr(x))))
#define is_atom(x) ((x!=NULL)&&(((x)->kind)==atom_k))
#define is_text(x) ((x!=NULL)&&(((x)->kind)==text_k))
#define is_num(x) ((x!=NULL)&&(((x)->kind)==num_k))
#define can_symbolize(x) ((is_atom(x))||(is_text(x)))
#define symbol_of(x) ((can_symbolize(x))?((char*)(((atom_t*)(x))->data)):"")
#define symbol_is(x,y) (strcmp(symbol_of(x), (y))==0)
#define args_of(x) (cdr(x))
#define proc_is(x,y) (strcmp((symbol_of(car(x))), (y))==0)
#define is_err(x) ((is_cons(x))&&(strcmp(symbol_of(car(x)),"error")==0))
#define get_numerator(x) ((is_num(x))?(((num_t*)x)->numerator):0)
#define get_denominator(x) ((is_num(x))?(((num_t*)x)->denominator):1)
#define is_grid(x) ((is_cons(x))&&(symbol_is(car(x),"&ready-grid")))
#define is_gen(x) ((x!=NULL)&&(((x)->kind)==gen_k))
#define is_litpair(x) ((is_cons(x))&&(strcmp(symbol_of(car(x)),"pair")==0))
#define is_vaukin(x) ((proc_is((x),"mac"))||(proc_is((x),"&vau"))||(proc_is((x),"def"))||(proc_is((x),"&lambda")))
#define is_recursive_cell(x) ((is_cons(x)) || (is_grid(x)))

#define is_whitespace(x) ((x)==' '||(x)=='\t'||(x)=='\n'||(x)=='\v'||(x)==',')
#define is_quotation(x) ((x)=='\''||(x)=='"'||(x)=='`')
#define is_opener(x) (((x)=='(')||((x)=='[')||((x)=='{'))
#define is_closer(x) (((x)==')')||((x)==']')||((x)=='}'))
#define is_paren(x) ((is_opener(x))||(is_closer(x)))
#define is_infix(x) (((x)==':')||((x)=='.')||((x)=='|'))
#define is_boundary(x) ((is_whitespace(x))||(is_quotation(x)||(is_paren(x))||(is_infix(x))))
#define is_big_infixer(x) (((x)=='-')||((x)=='~')||((x)=='<')||((x)=='>'))

#define advance_down_list(x,y) ((cdr(y)==x)?NULL:cdr(y))

#define attach(x,y) (accum((x),spawn_pair((y), NULL)))

#define INS_TO_CTX(x,y,z) ((x)=(spawn_pair((spawn_pair((y),(z))),(x))))
#define ADD_TO_CTX(x,y,z) (INS_TO_CTX((x),(spawn_symbol(y)),(z)))

cell_t *trueConstant,
    *lambdaConstant,
    *truePredicate,
    *foldConstant,
    *gridConstructor,
    *gridQuoter,
    *remainingArgsConstant,
    *localArgsConstant,
    *particulateCar,
    *particulateCdr,
    *atomKindConstant,
    *numKindConstant,
    *textKindConstant,
    *consKindConstant,
    *listKindConstant,
    *gridKindConstant,
    *vaukinKindConstant,
    *genKindConstant,
    *listConstructorConstant,
    *errorKindConstant,
    *applyProcedureConstant,
    *pairProcedureConstant,
    *dotOpConstant,
    *redirOpConstant,
    *notFoundConstant;

#endif
