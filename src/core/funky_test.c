#include "funky_core.h"

int fail(const char *msg) {
    printf("%s\n", msg);
    return 1;
}

int failnum(const char *msg, const long nm) {
    printf("%s: %ld\n", msg, nm);
    return 1;
}

int test0(void) {
    cell_t *num = spawn_symbol("123123123");
    cell_t *txt = spawn_text("soon the battle will be over");
    cell_t *foo = spawn_symbol("foo");
    cell_t *lst = spawn_pair(foo, spawn_pair(num, spawn_pair(txt, NULL)));
    cell_t *nil = spawn_symbol(NULL);
    cell_t *nul = spawn_symbol("");
    cell_t *emp = spawn_text("");
    cell_t *nested = spawn_pair(foo, spawn_pair(lst, spawn_pair(nul, spawn_pair(nil, spawn_pair(emp, spawn_pair(foo, NULL))))));
    if(emp == NULL)
        return fail("Empty string shouldn't be NULL");
    if(nil != NULL || nul != NULL)
        return fail("Empty symbols should be NULL");
    print(num);
    print(txt);
    print(lst);
    print(nested);
    destroy(lst);
    GC_gcollect();
    return 0;
}

int test1(void) {
    cell_t *ten = spawn_num("10");
    cell_t *pi = spawn_num("3.141596");
    num_t *this;
    if(ten->kind != num_k)
        return fail("10 should be a number type");
    this = (num_t*)ten;
    if(this->numerator != 10)
        return fail("Didn't parse 10 correctly.");
    if(this->denominator != 1)
        return fail("10 should have a 1 denominator");
    this = (num_t*)pi;
    if(this->numerator != 785399 || this->denominator != 250000) {
        printf("PI: %ld / %ld\n", this->numerator, this->denominator);
        return fail("Didn't parse pi correctly.");
    }
    printf("TEN = ");
    print(ten);
    printf("PI = ");
    print(pi);
    return 0;
}

int test2(void) {
    buffer_t *buffer = spawn_buffer(16);
    push_char(buffer, 'a');
    push_char(buffer, 'b');
    push_char(buffer, 'c');
    push_char(buffer, 'd');
    push_char(buffer, 'e');
    printf("Buffer length %d: %s\n", buffer->len, buffer->content);
    if(buffer->len != 5)
        return fail("'abcde' should have length of 5");

    del_char(buffer);
    printf("Buffer length %d: %s\n", buffer->len, buffer->content);
    if(buffer->len != 4)
        return fail("'abcd' should have length of 4");

    del_chars(buffer, 2);
    printf("Buffer length %d: %s\n", buffer->len, buffer->content);
    if(buffer->len != 2)
        return fail("'ab' should have length of 2");
    return 0;
}

int test3(void) {
    num_t *got = spawn_number("1.13e3");
    if(got == NULL)
        return fail("Valid scientific number failed to read in");
    if(got->numerator != 1130)
        return failnum("Numerator is wrong", got->numerator);
    if(got->denominator != 1)
        return failnum("Denominator is wrong", got->denominator);
    destroy((cell_t*)got);

    got = spawn_number("2.71828");
    if(got == NULL)
        return fail("2.71828 did not read in");
    if(got->numerator != 67957)
        return fail("2.71828 did not set correct numerator");
    if(got->denominator != 25000)
        return fail("2.71828 didn't have correct denominator");

    return 0;
}

int run_tests(void) {
    printf("Running static tests...\n");
    return test0() + test1() + test2() + test3();
}

int main(int argc, char **argv) {
    return run_tests();
}
