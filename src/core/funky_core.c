#include "funky_core.h"

static long gcf(long a, long b) {
    while(a > 0 && b > 0) {
        if(a > b)
            a %= b;
        else
            b %= a;
    }
    return a | b;
}

static int string_is_valid_number(const char *maybeNum) {
    char *endptr;
    long double got;
    if(*maybeNum=='\0')
        return 0;
    got=strtold(maybeNum, &endptr);
    return *endptr == '\0';
}

atom_t *spawn_atomic(const size_t size, const kind_t newKind) {
    atom_t *spawned;
    if(newKind == num_k || newKind == cons_k || newKind == gen_k)
        return NULL;
    spawned = GC_MALLOC(sizeof(atom_t));
    spawned->kind = newKind;
    spawned->data = GC_MALLOC(size);
    spawned->max = size;
    return spawned;
}

atom_t *spawn_atom(const char *smbl, const kind_t newKind) {
    atom_t *spawned;
    size_t strSize;
    if(!smbl || (!*smbl && newKind != text_k))
        return NULL;
    strSize = strlen(smbl) + 1;
    spawned = spawn_atomic(sizeof(char)*strSize, newKind);
    if(spawned && spawned->data)
        strlcpy(spawned->data, smbl, strSize);
    return spawned;
}

num_t *spawn_numeric(const long numerator, const long denominator) {
    num_t *spawned = GC_MALLOC(sizeof(num_t));
    spawned->kind = num_k;
    spawned->denominator = (denominator!=0) ? denominator : 1;
    spawned->numerator = numerator;
    return spawned;
}

static num_t *simplify_number(long num, long denom) {
    long use;
    denom = (denom==0) ? 1 : denom;
    if(num%denom == 0)
        return spawn_numeric(num/denom, 1);
    use = gcf(num, denom);
    use = (use==0) ? 1 : use;
    return spawn_numeric(num/use, denom/use);
}

num_t *spawn_number(const char *numberText) {
    long numerator=0, denominator=1;
    char *endptr;
    long double got;
    unsigned long int max;
    if(*numberText=='\0')
        return NULL;
    max = strlen(numberText)*2;
    got = strtold(numberText, &endptr);
    if(endptr != NULL && *endptr!='\0')
        return NULL;
    while((fmodl(got, 1)!=0.0) && max>0) {
        got *= 10;
        denominator *= 10;
        --max;
    }
    numerator = (long)got;
    return simplify_number(numerator, denominator);
}

pair_t *spawn_cons(cell_t *el1, cell_t *el2, const kind_t knd) {
    pair_t *spawned = GC_MALLOC(sizeof(pair_t));
    spawned->kind = knd;
    spawned->car = el1;
    spawned->cdr = el2;
    return spawned;
}

static void del_cons(pair_t *delMe) {
    GC_FREE(delMe);
}

static void obliterate_cons(pair_t *delMe) {
    destroy(delMe->car);
    destroy(delMe->cdr);
    del_cons(delMe);
}

static void del_atom(atom_t *delMe) {
    if(delMe->data)
        GC_FREE(delMe->data);
    GC_FREE(delMe);
}

static void del_num(num_t *delMe) {
    GC_FREE(delMe);
}

void destroy(cell_t *delMe) {
    if(!delMe)
        return;
    switch(delMe->kind) {
        case cons_k:
            obliterate_cons((pair_t*) delMe);
            return;
        case num_k:
            del_num((num_t*) delMe);
            return;
        case atom_k:
        case text_k:
        case mem_k:
        default:
            del_atom((atom_t*) delMe);
    }
}

static void depict_text(FILE *dst, const atom_t *this) {
    unsigned int idx;
    char *symbol;
    if(!this)
        return;
    fputc('"', dst);
    symbol = (char *)this->data;
    for(idx=0; symbol && symbol[idx]; ++idx) {
        if(symbol[idx]=='"')
            fputc('\\', dst);
        fputc(symbol[idx], dst);
    }
    fputc('"', dst);
}

static void depict_atom(FILE *dst, const atom_t *this) {
    char *symbol = (char *)this->data;
    fprintf(dst, "%s", (symbol) ? symbol : "nil");
}

static void depict_num(FILE *dst, const num_t *this) {
    if(this->denominator != 1)
        fprintf(dst, "(number ");
    fprintf(dst, "%ld", this->numerator);
    if(this->denominator != 1)
        fprintf(dst, " %ld)", this->denominator);
}

static void depict_grid(FILE *dst, const pair_t *this) {
    cell_t *next = this->cdr;
    fprintf(dst, "(grid ");
    while(next) {
        depict_this(dst, car(next));
        next = cdr(next);
        if(next)
            fprintf(dst, " ");
    }
    fprintf(dst, ")");
}


static void depict_cons(FILE *dst, const pair_t *this) {
    cell_t *next;
    unsigned int isPair;
    if(is_grid((cell_t*)this)) {
        depict_grid(dst, this);
        return;
    }
    fprintf(dst, "(");
    isPair = this->cdr && !is_cons(this->cdr);
    if(this->kind == gen_k)
        fprintf(dst, "gen ");
    else if(isPair)
        fprintf(dst, "pair ");
    if(this->kind == gen_k)
        fprintf(dst, "...");
    else
        depict_this(dst, this->car);
    next = this->cdr;
    while(next) {
        fprintf(dst, " ");
        if(next->kind != cons_k) {
            depict_this(dst, next);
            break;
        }
        depict_this(dst, car(next));
        next = cdr(next);
    }
    fprintf(dst, ")");
}

static void depict_this(FILE *dst, const cell_t *this) {
    if(!dst)
        dst = stdout;
    if(!this) {
        fprintf(dst, "nil");
        return;
    }
    switch(this->kind) {
        case gen_k:
        case cons_k:
            depict_cons(dst, (pair_t*)this);
            return;
        case text_k:
            depict_text(dst, (atom_t*)this);
            return;
        case num_k:
            depict_num(dst, (num_t*)this);
            return;
        case atom_k:
        default:
            depict_atom(dst, (atom_t*)this);
    }
}

void depict(FILE *dst, const cell_t *this) {
    if(this == notFoundConstant)
        return;
    depict_this(dst, this);
    fprintf(dst, "\n");
}

cell_t *cdr(const cell_t *this) {
    pair_t *subj;
    if(!this || this->kind != cons_k)
        return NULL;
    subj = (pair_t *)this;
    return subj->cdr;
}

cell_t *car(const cell_t *this) {
    pair_t *subj;
    if(!this || (!is_cons(this) && !is_gen(this)))
        return NULL;
    subj = (pair_t *)this;
    return subj->car;
}

buffer_t *spawn_buffer(size_t wantedSize) {
    buffer_t *output = GC_MALLOC(sizeof(buffer_t));
    output->size = (wantedSize>2) ? wantedSize : 2;
    output->content = GC_MALLOC(sizeof(char) * output->size);
    output->content[0] = '\0';
    output->len = 0;
    output->isNumeric = 0;
    return output;
}

void resize_buffer_if_needed(buffer_t *dest) {
    if(!dest || dest->len < dest->size - 2)
        return;
    dest->size += sizeof(char)*32;
    dest->content = GC_REALLOC(dest->content, dest->size);
}

static int can_read_as_num(const int input, const int len) {
    char valid[15] = ".0123456789-+Ee";
    unsigned int min = (len>1) ? 0 : 1;
    unsigned int max = (len>1) ? 11 : 15;
    unsigned int idx;
    for(idx=min;idx<max;++idx)
        if(valid[idx] == input)
            return 1;
    return 0;
}

int push_char(buffer_t *dest, const int input) {
    if(!dest)
        return input;
    resize_buffer_if_needed(dest);
    if(dest->content)
        dest->content[dest->len++] = input;
    dest->isNumeric = can_read_as_num(input, dest->len);
    return input;
}

void delete_buffer(buffer_t *this) {
    if(!this)
        return;
    if(this->content)
        GC_FREE(this->content);
    GC_FREE(this);
}

void clear_buffer(buffer_t *this) {
    if(!this || !this->content || this->len<1)
        return;
    this->len = 0;
    this->isNumeric = 0;
    memset(this->content, '\0', this->size);
}

void del_char(buffer_t *this) {
    if(!this || !this->content || this->len<1)
        return;
    this->len-=1;
    this->content[this->len]='\0';
}

void del_chars(buffer_t *this, const unsigned int quant) {
    unsigned int idx;
    for(idx=0;idx<quant;idx++)
        del_char(this);
}

int ends_with(buffer_t *this, const char *suffix) {
    int suffixLen, suffixDiff;
    char *tail;
    if(!this || !suffix || !this->content)
        return 0;
    suffixLen = strlen(suffix);
    suffixDiff = (this->len)-suffixLen;
    if(suffixDiff < 0)
        return 0;
    tail = this->content + suffixDiff;
    return strcmp(tail, suffix)==0;
}

void set_car(cell_t *owner, cell_t *newCar) {
    pair_t *this;
    if(!owner || owner->kind != cons_k)
        return;
    this = (pair_t *)owner;
    this->car = newCar;
}

void set_cdr(cell_t *owner, cell_t *newCdr) {
    pair_t *this;
    if(!owner || owner->kind != cons_k)
        return;
    this = (pair_t*)owner;
    this->cdr = newCdr;
}

static void accum(cell_t *owner, cell_t *latest) {
    if(!owner || !latest)
        return;
    set_cdr(cdr(owner), latest);
    set_cdr(owner, latest);
    if(!car(owner))
        set_car(owner, latest);
}

static void accumulate(cell_t *owner, const kind_t kind, const char *label) {
    if(!owner)
        return;
    if(strlen(label) > 0)
        accum(owner, spawn_pair((cell_t*)spawn_atom(label, kind), NULL));
    else
        accum(owner, spawn_pair(NULL, NULL));
}

static int keep_reading(const char input, const char term, const char prev) {
    return input != term || prev == '\\';
}

static void read_txt_chunk(FILE *src, buffer_t *buffer, const char term) {
    int input, prev;
    clear_buffer(buffer);
    prev = ' ';
    while(((input = fgetc(src)) != EOF) && keep_reading(input, term, prev)) {
        switch(input) {
            case '\\':
                if(prev != '\\')
                    break;
            default:
                push_char(buffer, input);
        }
        prev = input;
    }
}

static void tidy_up_buf(buffer_t *buffer, cell_t *accumulator) {
    if(!buffer || !accumulator || buffer->len < 1)
        return;
    if(buffer->isNumeric && string_is_valid_number(buffer->content)) {
        attach(accumulator, spawn_num(buffer->content));
        clear_buffer(buffer);
        return;
    }
    if(strcmp(buffer->content, "nil")==0)
        attach(accumulator, NULL);
    else
        accumulate(accumulator, atom_k, buffer->content);
    clear_buffer(buffer);
}

static void backtrack(buffer_t *buffer, cell_t *accumulator, const char *symbol) {
    unsigned int len;
    if(!buffer || !accumulator || !ends_with(buffer, symbol))
        return;
    len = strlen(symbol);
    del_chars(buffer, len);
    buffer->isNumeric = string_is_valid_number(buffer->content);
    tidy_up_buf(buffer, accumulator);
    accumulate(accumulator, atom_k, symbol);
}

static int continue_reading(const int input, const int isNested) {
    return (input != EOF) && ((input != '\n') || (isNested));
}

static cell_t *read_sexprs(FILE *src, buffer_t *buffer, int *lastChar) {
    int input;
    cell_t *original = spawn_pair(NULL, NULL);
    cell_t *accumulator = original;
    cell_t *stack = NULL;
    while(continue_reading((input = fgetc(src)), stack != NULL)) {
        if(input=='#') {
            while((input = fgetc(src)) != EOF && input != '\n');
            continue;
        }

        if(input=='.' && buffer->isNumeric) {
            push_char(buffer, input);
            continue;
        }

        if(is_infix(input)) {
            tidy_up_buf(buffer, accumulator);
            push_char(buffer, input);
            accumulate(accumulator, atom_k, buffer->content);
            clear_buffer(buffer);
            continue;
        }

        if(is_opener(input)) {
            tidy_up_buf(buffer, accumulator);
            stack = spawn_pair(accumulator, stack);
            accumulator = spawn_pair(NULL, NULL);
            if(input == '[')
                attach(accumulator, listConstructorConstant);
            if(input == '{')
                attach(accumulator, gridConstructor);
            continue;
        }

        if(is_closer(input) && stack!=NULL) {
            tidy_up_buf(buffer, accumulator);
            attach(car(stack), car(accumulator));
            accumulator = car(stack);
            stack = cdr(stack);
            continue;
        }

        if(is_quotation(input)) {
            tidy_up_buf(buffer, accumulator);
            read_txt_chunk(src, buffer, input);
            accumulate(accumulator, text_k, buffer->content);
            clear_buffer(buffer);
            continue;
        }

        if(is_whitespace(input)) {
            tidy_up_buf(buffer, accumulator);
            continue;
        }

        push_char(buffer, input);

        backtrack(buffer, accumulator, "->");
        backtrack(buffer, accumulator, "~>");
        backtrack(buffer, accumulator, "<~");
        backtrack(buffer, accumulator, "<-");
    }
    tidy_up_buf(buffer, accumulator);
    *lastChar = input;
    return car(original);
}

static cell_t *read_lisp_chunk(FILE *src, int *lastInput) {
    if(!src)
        return NULL;
    buffer_t *buffer = spawn_buffer(512);
    cell_t *output = read_sexprs(src, buffer, lastInput);
    delete_buffer(buffer);
    return output;
}

static cell_t *unpack_infix(cell_t *op, cell_t *lft, cell_t *ryt) {
    if(symbol_is(op, "->"))
        return spawn_doublet(ryt, lft);
    if(symbol_is(op, "<-"))
        return spawn_doublet(lft, ryt);
    if(symbol_is(op, "~>"))
        return spawn_triplet(applyProcedureConstant, ryt, lft);
    if(symbol_is(op, "<~"))
        return spawn_triplet(applyProcedureConstant, lft, ryt);
    if(symbol_is(op, ":"))
        return spawn_triplet(pairProcedureConstant, lft, ryt);
    if(symbol_is(op, "."))
        return spawn_triplet(dotOpConstant, lft, ryt);
    if(symbol_is(op, "|"))
        return spawn_triplet(redirOpConstant, lft, ryt);
    return spawn_errr("Unknown infix operator", op);
}

static cell_t *desugar_syntax(cell_t *ast, const char *token) {
    cell_t *cur = ast;
    cell_t *left, *right, *op, *beyond;
    while(cur) {
        left = car(cur);
        op = car(cdr(cur));
        right = car(cdr(cdr(cur)));
        beyond = cdr(cdr(cdr(cur)));

        left = (is_cons(left)) ? desugar_syntax(left, token) : left;
        right = (is_cons(right)) ? desugar_syntax(right, token) : right;

        if(symbol_is(left, token)) {
            set_car(cur, unpack_infix(left, NULL, op));
            set_cdr(cur, cdr(cdr(cur)));
            continue;
        }

        if(symbol_is(op, token)) {
            set_car(cur, unpack_infix(op, left, right));
            set_cdr(cur, beyond);
            continue;
        }

        cur = advance_down_list(ast, cur);
    }
    return ast;
}

cell_t *comprehend_syntax(cell_t *ast) {
    unsigned int idx;
    const char *ops[7] = {".", ":", "->", "~>", "<-", "<~", "|"};
    for(idx=0;idx<7;++idx)
        ast = desugar_syntax(ast, ops[idx]);
    return ast;
}

cell_t *context_lookup(cell_t *head, cell_t *want, cell_t *fallback) {
    cell_t *kvp, *cur = head;
    const char *symb = symbol_of(want);
    if(!want || !symb || *symb=='\0')
        return want;
    if(strcmp(symb, "true")==0)
        return trueConstant;
    while(cur) {
        kvp = car(cur);
        if(symbol_is(car(kvp), symb))
            return cdr(kvp);
        cur = advance_down_list(head, cur);
    }
    if(symb && *symb=='&')
        return want;
    return fallback;
}

cell_t *context_trample(cell_t *head, cell_t *want, cell_t *valNew) {
    cell_t *kvp, *cur = head;
    const char *symb = symbol_of(want);
    if(!want || !symb || *symb=='\0')
        return NULL;
    while(cur) {
        kvp = car(cur);
        if(symbol_is(car(kvp), symb)) {
            set_cdr(kvp, valNew);
            return valNew;
        }
        cur = advance_down_list(head, cur);
    }
    return NULL;
}

static cell_t *trample_state(cell_t *context, cell_t *expr) {
    cell_t *varName = car(cdr(expr));
    cell_t *val2set = car(cdr(cdr(expr)));
    char *symb;
    if(!varName || !val2set)
        return spawn_errr("Unable to reassign variable", expr);
    symb = symbol_of(varName);
    if(symb && *symb == '&')
        return spawn_err("Cannot trample variables starting with ampersand!");
    return context_trample(context, varName, eval(context, val2set));
}

static cell_t *core_peek_gen(cell_t *context, cell_t *expr) {
    cell_t *this = is_gen(expr) ? expr : eval(context, car(cdr(expr)));
    if(get_kind_of(this) != gen_k)
        return spawn_errr("Can't peek at non-gen", this);
    return car(this);
}

static cell_t *core_decimal_symbol(cell_t *context, cell_t *expr) {
    num_t *this;
    cell_t *nmbr;
    double dec;
    char holder[1024];
    if(!expr || !context)
        return NULL;
    nmbr = eval(context, car(cdr(expr)));
    if(!nmbr || !is_num(nmbr))
        return spawn_errr("Unable to convert to decimal number", expr);
    this = (num_t*)nmbr;
    dec = ((double)this->numerator / (double)this->denominator);
    snprintf(holder, 1023, "%f", dec);
    return (cell_t*)spawn_atom(holder, atom_k);
}

static cell_t *core_is_defined(cell_t *context, cell_t *expr) {
    cell_t *cur = car(args_of(expr));
    if(cur && (context_lookup(context, cur, notFoundConstant) != notFoundConstant))
        return trueConstant;
    return NULL;
}

static cell_t *core_get_type(cell_t *context, cell_t *expr) {
    cell_t *cur = eval(context, car(args_of(expr)));
    if(!cur)
        return NULL;
    switch(cur->kind) {
        case atom_k:
            return atomKindConstant;
        case num_k:
            return numKindConstant;
        case text_k:
            return textKindConstant;
        case gen_k:
            return genKindConstant;
        case cons_k:
            if(is_err(cur))
                return errorKindConstant;
            if(is_vaukin(cur))
                return vaukinKindConstant;
            if(is_grid(cur))
                return gridKindConstant;
            if(is_cons(cdr(cur)) || cdr(cur)==NULL)
                return listKindConstant;
            return consKindConstant;
        default:
            return NULL;
    }
}

static cell_t *core_cons_getter(cell_t *context, cell_t *head) {
    cell_t *op = car(head);
    cell_t *subj = (op) ? eval(context, car(cdr(head))) : NULL;
    if(subj && symbol_is(op, "&cons-car"))
        return car(subj);
    if(subj && symbol_is(op, "&cons-cdr"))
        return cdr(subj);
    return NULL;
}

static cell_t *core_is_number(cell_t *context, cell_t *expr) {
    cell_t *args=args_of(expr);
    char *this;
    while(args) {
        if(!is_num(eval(context, car(args))))
            return NULL;
        args = cdr(args);
    }
    return trueConstant;
}

static cell_t *core_generate_rational_number(cell_t *context, cell_t *expr) {
    cell_t *parts=args_of(expr);
    cell_t *left = car(parts);
    cell_t *right = car(cdr(parts));
    if(!is_num(left))
        return spawn_errr("Unable to generate number!", parts);
    if(!is_num(right))
        return left;
    num_t *num = (num_t *)left;
    num->denominator = ((num_t*)right)->numerator;
    return left;
}

static cell_t *perform_text_operation(cell_t *source, cell_t *operation) {
    cell_t *operands = cdr(car(operation));
    cell_t *thisOp = car(car(operation));
    if(symbol_is(thisOp, "replace"))
        return spawn_err("Text replacement isn't implemented");
    if(symbol_is(thisOp, "sub"))
        return spawn_err("Text substring isn't implemented");
    if(symbol_is(thisOp, "split"))
        return spawn_err("Text splitting isn't implemented");
    if(symbol_is(thisOp, "len"))
        return spawn_err("Text length isn't implemented");
    if(symbol_is(thisOp, "upper"))
        return spawn_err("Text upper casing isn't implemented");
    if(symbol_is(thisOp, "lower"))
        return spawn_err("Text lower casing isn't implemented");
    return spawn_errr("Unknown text operation!", thisOp);
}

static cell_t *get_from_grid(cell_t *head, const char *keyName) {
    cell_t *cur = head;
    while(keyName && (cur=advance_down_list(head, cur)))
        if(symbol_is(car(car(cur)), keyName))
            return cdr(car(cur));
    return NULL;
}

static cell_t *perform_number_operation(cell_t *source, cell_t *operation) {
    return spawn_errr("Numeric operation not implemented for", source);
}

static cell_t *core_dot_operation(cell_t *context, cell_t *expr) {
    cell_t *source=eval(context, car(args_of(expr)));
    char *wantedThing = symbol_of(car(cdr(args_of(expr))));
    source = eval(context, source);
    if(is_grid(source))
        return get_from_grid(source, wantedThing);
    if(is_text(source))
        return perform_text_operation(source, cdr(args_of(expr)));
    return spawn_errr("Illegal 'dot' operation in", source);
}

static cell_t *core_print(cell_t *context, cell_t *expr) {
    cell_t *cur = expr;
    cell_t *showMe;
    int noPad = (proc_is(expr, "print-no-padding"));
    int shallEval = (noPad || (proc_is(expr, "print")) || (proc_is(expr, "echo")));

    while((cur = advance_down_list(expr, cur))) {
        showMe = ((shallEval)?(eval(context, car(cur))):(car(cur)));
        if(is_text(showMe))
            printf("%s",symbol_of(showMe));
        else
            depict_this(stdout, showMe);
        if(!noPad)
            printf(" ");
    }
    if(!noPad)
        printf("\n");
    return showMe;
}

static cell_t *core_echo(cell_t *context, cell_t *expr) {
    core_print(context, expr);
    return notFoundConstant;
}

static cell_t *core_pair(cell_t *context, cell_t *expr) {
    return spawn_pair(eval(context, car(args_of(expr))),
                      eval(context, car(cdr(args_of(expr)))));
}

static int same_numbers(cell_t *left, cell_t *right) {
    long leftn, leftd, rightn, rightd, hold;
    if((!is_num(left)) || (!is_num(right)))
        return 0==1;
    leftn = get_numerator(left);
    leftd = get_denominator(left);
    rightn = get_numerator(right);
    rightd = get_denominator(right);
    if(leftd != rightd) {
        leftn *= rightd;
        rightn *= leftd;
        hold = rightd;
        rightd *= leftd;
        leftd *= hold;
    }
    return (leftd == rightd && leftn == rightn);
}

static int cells_match(cell_t *lcur, cell_t *rcur) {
    do {
        if(lcur == rcur)
            return 1;
        if((!lcur || !rcur) || (lcur->kind != rcur->kind))
            return 0;
        if(is_num(lcur) && !same_numbers(lcur, rcur))
            return 0;
        if(can_symbolize(lcur) && (!symbol_is(lcur, symbol_of(rcur))) )
            return 0;
        if(!cells_match(car(lcur), car(rcur)))
            return 0;
        lcur = cdr(lcur);
        rcur = cdr(rcur);
    } while(lcur && rcur);
    return lcur == rcur;
}

static int determine_ineq_num(cell_t *op, long left, long right) {
    if(symbol_is(op, ">"))
        return left > right;
    if(symbol_is(op, "<"))
        return left < right;
    if(symbol_is(op, ">="))
        return left >= right;
    if(symbol_is(op, "<="))
        return left <= right;
    return left == right;
}

static int determine_ineq(cell_t *op, num_t *left, num_t *right) {
    long lNum=left->numerator,
        lDen=left->denominator,
        rNum=right->numerator,
        rDen=right->denominator;
    return determine_ineq_num(op,
                              ((lDen == rDen) ? lNum : (lNum*rDen)),
                              ((lDen == rDen) ? rNum : (rNum*lDen)));
}

static cell_t *core_inequality(cell_t *context, cell_t *head) {
    cell_t *cur = head;
    cell_t *op = car(head);
    cell_t *left, *right;

    while((cur = advance_down_list(head, cur))) {
        left = eval(context, car(cur));
        if(!is_num(left))
            return spawn_errr("Non-numeric type in comparison", left);
        right = eval(context, car(cdr(cur)));
        if(!right)
            continue;
        if(!is_num(right))
            return spawn_errr("Non-numeric type in comparison", right);
        if(!determine_ineq(op, (num_t*)left, (num_t*)right))
            return NULL;
    }
    return trueConstant;
}

static cell_t *core_equality(cell_t *context, cell_t *head) {
    cell_t *cur = head, *first=eval(context, car(cdr(head)));
    while((cur = advance_down_list(head, cur)))
        if(!cells_match(first, eval(context, car(cur))))
            return NULL;
    return trueConstant;
}

static cell_t *core_truthy_predicate(cell_t *context, cell_t *head) {
    cell_t *out, *cur = head;
    int shallAnd = proc_is(cur, "and");
    while((cur = advance_down_list(head, cur))) {
        out = eval(context, car(cur));
        if(!out)
            return NULL;
    }
    return (shallAnd) ? out : trueConstant;
}

static cell_t *core_boolean_or(cell_t *context, cell_t *head) {
    cell_t *cur = head;
    cell_t *out = NULL;
    while((cur = advance_down_list(head, cur))) {
        out = eval(context, car(cur));
        if(out)
            return out;
    }
    return NULL;
}

static cell_t *core_boolean_not(cell_t *context, cell_t *head) {
    cell_t *cur = head;
    while((cur=advance_down_list(head, cur)))
        if(eval(context, car(cur)))
            return NULL;
    return trueConstant;
}

static cell_t *core_quote_full_expr_back(cell_t *context, cell_t *expr) {
    return expr;
}

static cell_t *quote_back_monadic(cell_t *context, cell_t *expr) {
    return car(args_of(expr));
}

static cell_t *core_assign_to_context(cell_t *context, cell_t *expr) {
    cell_t *varName = car(args_of(expr));
    cell_t *varValu = eval(context, car(cdr(args_of(expr))));
    const char *symb = symbol_of(varName);
    if(!varName)
        return NULL;
    if(symb && *symb=='&' && !symbol_is(car(expr), "&set!"))
        return spawn_err("Cannot assign symbols starting with ampersands.");
    set_cdr(context, spawn_pair(spawn_pair(car(car(context)), cdr(car(context))), cdr(context)));
    set_car(context, spawn_pair(varName, varValu));
    return varValu;
}

static cell_t *core_begin_body(cell_t *context, cell_t *head) {
    cell_t *out = NULL, *cur = head;
    unsigned int iters = 0;
    while(cur) {
        out = eval(context, car(cur));
        if(is_err(out))
            return out;
        cur = advance_down_list(head, cur);
    }
    return out;
}

static cell_t *core_cond(cell_t *context, cell_t *head) {
    cell_t *cur = cdr(head), *got, *conditional, *consequent;
    while(cur) {
        got = car(cur);
        if(symbol_is(car(got), "pair"))
            got = cdr(got);
        conditional = car(got);
        consequent = car(cdr(got));
        if(eval(context, conditional))
            return eval(context, consequent);
        cur = advance_down_list(head, cur);
    }
    return NULL;
}

static cell_t *core_generate_list(cell_t *context, cell_t *head) {
    pair_t myAccum = {cons_k, NULL, NULL};
    cell_t *accumulator = (cell_t*)&myAccum, *cur = head;
    while((cur = advance_down_list(head, cur)))
        attach(accumulator, eval(context, car(cur)));
    return car(accumulator);
}

static cell_t *core_construct_grid(cell_t *context, cell_t *head) {
    cell_t *key, *val, *content = NULL, *cur = head;
    while((cur = advance_down_list(head, cur))) {
        key = car(cdr(car(cur)));
        val = car(cdr(cdr(car(cur))));
        content = spawn_pair(spawn_pair(key, eval(context, val)), content);
    }
    return spawn_pair(gridQuoter, content);
}

static cell_t *core_substitute(cell_t *context, cell_t *expr) {
    cell_t *head = (symbol_is(car(expr), "&substitute")) ? car(cdr(expr)) : expr;
    pair_t myAccum = {cons_k, NULL, NULL};
    cell_t *accumulator = (cell_t*)&myAccum;
    cell_t *cur, *got;

    if(is_atom(head))
        return context_lookup(context, head, head);
    if(!is_recursive_cell(head))
        return head;

    for(cur=head; cur; cur = advance_down_list(head, cur))
        attach(accumulator, core_substitute(context, car(cur)));
    got = car(accumulator);
    if(got)
        got->kind = head->kind;

    return got;
}

static cell_t *core_construct_genesis(cell_t *context, cell_t *head) {
    cell_t *out = head;
    if(!out)
        return out;
    if((symbol_is(car(out), "gen")) || (symbol_is(car(out), "GEN")))
        out = cdr(out);
    return spawn_gen(core_substitute(context, car(out)), NULL);
}

static cell_t *partially_apply_vau(cell_t *context,
                                   cell_t *unsupplied,
                                   cell_t *expr) {
    return spawn_pair(lambdaConstant,
                      spawn_pair(unsupplied,
                                 core_substitute(context,
                                                 cdr(cdr(car(expr))))));
}

static cell_t *inner_let(cell_t *ocontext, cell_t *expr, funky_fn fn) {
    cell_t *context = ocontext, *head = car(args_of(expr)), *got;
    cell_t *cur = head;
    if(strcmp(symbol_of(car(cur)), "grid")==0)
        cur = cdr(cur);

    while(cur) {
        got = car(cur);
        got = (symbol_is(car(got), "pair")) ? cdr(got) : got;
        context = spawn_pair(spawn_pair(car(got), eval(context, car(cdr(got)))), context);
        cur = advance_down_list(head, cur);
    }
    return fn(context, cdr(args_of(expr)));
}

static cell_t *core_let_expr(cell_t *context, cell_t *expr) {
    return inner_let(context, expr, core_begin_body);
}

static cell_t *wrap_for_substitution(cell_t *context, cell_t *expr) {
    return core_substitute(context, car(expr));
}

static cell_t *core_let_substitute(cell_t *context, cell_t *expr) {
    return inner_let(context, expr, wrap_for_substitution);
}

static cell_t *sum_or_diff(cell_t *context, cell_t *head, long mult) {
    cell_t *cur = cdr(head);
    cell_t *got;
    num_t *this = (num_t*)eval(context, car(cur));
    long sum = this->numerator;
    long denom = this->denominator;
    long num, over;
    while((cur = advance_down_list(head, cur))) {
        got = eval(context, car(cur));
        if(!is_num(got))
            return spawn_errr("Non-numeric type", car(cur));
        this = (num_t*)got;
        num = this->numerator;
        over = this->denominator;
        if(over != denom) {
            num *= denom;
            over *= denom;
            sum *= this->denominator;
            denom *= this->denominator;
        }
        sum += (num*mult);
    }
    return (cell_t *)simplify_number(sum, denom);
}

static cell_t *times_or_div(cell_t *context, cell_t *head, char isDivision) {
    cell_t *cur = cdr(head);
    cell_t *got;
    num_t *this = (num_t*)eval(context, car(cur));
    long numer = this->numerator;
    long denom = this->denominator;
    while((cur = advance_down_list(head, cur))) {
        got = eval(context, car(cur));
        if(!is_num(got))
            return spawn_errr("Non-numeric type", car(cur));
        this = (num_t*)got;
        numer *= (isDivision) ? this->denominator : this->numerator;
        denom *= (isDivision) ? this->numerator : this->denominator;
    }
    return (cell_t *)simplify_number(numer, denom);
}

static cell_t *core_multiply_numbers(cell_t *context, cell_t *head) {
    return times_or_div(context, head, 0);
}

static cell_t *core_divide_numbers(cell_t *context, cell_t *head) {
    return times_or_div(context, head, 1);
}

static cell_t *core_sum_numbers(cell_t *context, cell_t *head) {
    return sum_or_diff(context, head, 1);
}

static cell_t *core_subtract_numbers(cell_t *context, cell_t *expr) {
    return sum_or_diff(context, expr, -1);
}

static cell_t *core_floor_number(cell_t *context, cell_t *expr) {
    cell_t *cur = cdr(expr);
    long num, den, off;
    if(!is_num(car(cur)))
        return spawn_errr("Not a numeric type:", car(cur));
    num = get_numerator(car(cur));
    den = get_denominator(car(cur));
    off = (num < 0) ? -1 : 1;
    if(den==1)
        return car(cur);
    while((num % den) != 0)
        num-=off;
    return (cell_t*)simplify_number(num, den);
}

static cell_t *core_modulous_numbers(cell_t *context, cell_t *expr) {
    cell_t *cur = cdr(expr);
    long num1, den1, num2, den2, hold;
    if(!is_num(car(cur)) || !is_num(car(cdr(cur))))
        return spawn_errr("Modulus needs exactly two numeric arguments", expr);
    num1 = get_numerator(car(cur));
    den1 = get_denominator(car(cur));
    cur = cdr(cur);
    num2 = get_numerator(car(cur));
    den2 = get_denominator(car(cur));
    if(den1 != den2) {
        hold = den1;
        den1 *= den2;
        num1 *= den2;
        den2 *= hold;
        num2 *= hold;
    }
    return (cell_t *)simplify_number(num1%num2, den1);
}

static cell_t *core_vau(cell_t *context, cell_t *expr) {
    cell_t *thisVauNm;
    cell_t *vauName = (is_atom(car(args_of(expr)))) ? car(cdr(expr)) : NULL;
    cell_t *operands = (!vauName) ?
        car(args_of(expr)) :
        car(cdr(args_of(expr)));
    if(operands!=NULL && !is_cons(operands))
        return spawn_errr("Given operands list invalid:", operands);
    if(vauName) {
        if(*symbol_of(vauName) == '&' && *symbol_of(car(expr)) != '&') {
            return spawn_errr("Procedure/macro names should not begin with &", expr);
        }
        set_cdr(expr, cdr(args_of(expr)));
        thisVauNm = spawn_pair(vauName, expr);
        set_cdr(context, spawn_pair(thisVauNm, cdr(context)));
    }
    return expr;
}

static int list_contains(cell_t *haystack, cell_t *needle) {
    cell_t *cur = haystack;
    while(cur) {
        if(symbol_is(car(cur), symbol_of(needle)))
            return 1;
        cur = advance_down_list(haystack, cur);
    }
    return 0;
}

static cell_t *list_dedup(cell_t *head) {
    pair_t myAccum = {cons_k, NULL, NULL};
    cell_t *accumulator=(cell_t*)&myAccum, *cur = head;
    while(cur) {
        if(!list_contains(car(accumulator), car(cur)))
            attach(accumulator, car(cur));
        cur = advance_down_list(head, cur);
    }
    return car(accumulator);
}

static cell_t *identify_unknowns(cell_t *context, cell_t *head) {
    cell_t *accumulator = spawn_pair(NULL, NULL), *got, *cur = head;
    do {
        if(is_atom(car(cur))) {
            got = context_lookup(context, car(cur), notFoundConstant);
            if(got != notFoundConstant) {
                set_car(cur, got);
                continue;
            }
            attach(accumulator, car(cur));
            continue;
        }
        if(is_cons(car(cur))) {
            got = identify_unknowns(context, car(cur));
            if(car(got)) {
                accum(accumulator, car(got));
                set_cdr(accumulator, cdr(got));
            }
            continue;
        }
    } while((cur = advance_down_list(head, cur)));
    if(!car(accumulator) && !cdr(accumulator))
        return NULL;
    return accumulator;
}

static cell_t *core_rectify(cell_t *context, cell_t *expr) {
    cell_t *body = cdr(expr);
    cell_t *unknowns = NULL;

    unknowns = car(identify_unknowns(context, body));
    if(!unknowns)
        return core_begin_body(context, body);

    unknowns = list_dedup(unknowns);

    return spawn_pair(lambdaConstant, spawn_pair(unknowns, body));
}

static cell_t *core_get_all_args(cell_t *context, cell_t *junk) {
    return context_lookup(context, localArgsConstant, NULL);
}

static cell_t *core_get_more_args(cell_t *context, cell_t *junk) {
    return context_lookup(context, remainingArgsConstant, NULL);
}

static cell_t *evaluate_all(cell_t *context, cell_t *these) {
    pair_t myAccum = {cons_k, NULL, NULL};
    cell_t *accumulator = (cell_t*)&myAccum;
    cell_t *cur = these, *got;
    do {
        got = eval(context, car(cur));
        attach(accumulator, got);
    } while((cur = advance_down_list(these, cur)));
    return car(accumulator);
}


static cell_t *core_run_vau(cell_t *ocontext, cell_t *expr) {
    pair_t myAccum = {cons_k, NULL, NULL};
    cell_t *context = ocontext;
    cell_t *accumulator = (cell_t*)&myAccum;
    cell_t *this = core_vau(context, car(expr));
    cell_t *ops, *args, *curOp, *curArg, *body;
    int shallEval = (car(this) == lambdaConstant) ||
        (proc_is(this, "def")) || (proc_is(this, "&lambda"));
    int shallPartial = 0;
    ops = car(cdr(this));
    body = cdr(cdr(this));
    if(is_atom(ops)) {
        ops = car(cdr(cdr(this)));
        body = cdr(body);
    }
    args = cdr(expr);
    curArg = args;
    for(curOp=ops; curOp; curOp = advance_down_list(ops, curOp)) {
        if(!curArg) {
            shallPartial = 1;
            break;
        }
        this = (shallEval) ? eval(context, car(curArg)) : car(curArg);
        context = spawn_pair(spawn_pair(car(curOp), this), context);
        attach(accumulator, spawn_pair(car(curOp), this));
        curArg = cdr(curArg);
    }

    curArg = (shallEval) ? evaluate_all(context, curArg) : curArg;

    context = spawn_pair(spawn_pair(localArgsConstant, args), context);
    context = spawn_pair(spawn_pair(remainingArgsConstant, curArg), context);

    return (shallPartial) ?
        partially_apply_vau(car(accumulator), curOp, expr) :
        core_begin_body(context, body);
}

static cell_t *apply_procedure(cell_t *context, cell_t *expr) {
    cell_t *proc = eval(context, car(cdr(expr)));
    cell_t *args = eval(context, car(cdr(cdr(expr))));
    while(symbol_is(proc, "&apply") && proc && args) {
        proc = car(args);
        args = cdr(args);
    }
    return eval(context, spawn_pair(proc, args));
}

static cell_t *get_context(cell_t *context, cell_t *ignored) {
    return context;
}

static cell_t *core_resolve_gen(cell_t *context, cell_t *expr) {
    cell_t *head = proc_is(expr, "&resolve-gen") ?
        eval(context, car(cdr(expr))) :
        cdr(expr);
    if(get_kind_of(head)!=gen_k)
        return eval(context, head);
    return eval(context, car(head));
}

fundamental_t fundamentals[] = {
    /* These need to be implemented.  For now, they just quote back. */
    {"&run-subshell", core_quote_full_expr_back},
    {"&redirect-process-output", core_quote_full_expr_back},

    /* These are implemented and need to be tested. */
    {"&add-numbers", core_sum_numbers},
    {"&all-args", core_get_all_args},
    {"&apply", apply_procedure},
    {"&assign", core_assign_to_context},
    {"&begin", core_begin_body},
    {"&cond", core_cond},
    {"&cons-getter", core_cons_getter},
    {"&construct-grid", core_construct_grid},
    {"&construct-list", core_generate_list},
    {"&divide-numbers", core_divide_numbers},
    {"&dot-operation", core_dot_operation},
    {"&echo", core_echo},
    {"&equality-operator", core_equality},
    {"&floor-number", core_floor_number},
    {"&gen", core_construct_genesis},
    {"&get-context", get_context},
    {"&get-type", core_get_type},
    {"&inequality-operator", core_inequality},
    {"&is-defined", core_is_defined},
    {"&lambda", core_vau},
    {"&let", core_let_expr},
    {"&modulus-numbers", core_modulous_numbers},
    {"&more-args", core_get_more_args},
    {"&multiply-numbers", core_multiply_numbers},
    {"&not", core_boolean_not},
    {"&num-to-decimal", core_decimal_symbol},
    {"&number", core_generate_rational_number},
    {"&number?", core_is_number},
    {"&or-operator", core_boolean_or},
    {"&pair", core_pair},
    {"&peek-at-gen", core_peek_gen},
    {"&print", core_print},
    {"&quote", quote_back_monadic},
    {"&rectify", core_rectify},
    {"&resolve-gen", core_resolve_gen},
    {"&substitute", core_substitute},
    {"&substitute-let", core_let_substitute},
    {"&subtract-numbers", core_subtract_numbers},
    {"&trample-state!", trample_state},
    {"&true?", core_truthy_predicate},
    {"&vau", core_vau},
    {NULL, NULL},
};

cell_t *eval(cell_t *context, cell_t *expr) {
    fundamental_t *curfn;
    cell_t *cur;
    int idx;

    for(idx=0;idx<100;++idx) {
        if(!expr || expr == notFoundConstant)
            return NULL;
        if(expr == trueConstant || is_text(expr) || is_num(expr))
            return expr;

        if(is_atom(expr))
            return context_lookup(context, expr, NULL);

        if(is_err(expr) || is_grid(expr) || is_gen(expr))
            return expr;

        if(is_atom(car(expr))) {
            if(!(cur = eval(context, car(expr))))
                return spawn_errr("Unknown procedure in", expr);
            if(is_err(cur))
                return cur;
            if(symbol_is(cur, "&eval")) {
                expr = car(args_of(expr));
                continue;
            }
            if(!is_vaukin(cur)) {
                for(curfn = fundamentals; (*curfn).name; curfn++)
                    if(strcmp((*curfn).name, symbol_of(cur))==0)
                        return (*curfn).fn(context, expr);
            }
            set_car(expr, cur);
            continue;
        }

        if(is_vaukin(car(expr)))
            return core_run_vau(context, expr);

        cur = eval(context, car(expr));
        set_car(expr, cur);
    }

    return spawn_errr("Cannot evaluate", expr);
}

cell_t *initial_context(void) {
    cell_t *ctx = NULL;

    cell_t *dotOp = spawn_symbol("&dot-operation");
    cell_t *assignmentOp = spawn_symbol("&assign");

    notFoundConstant = spawn_symbol("&set!");
    gridConstructor = spawn_symbol("grid");
    gridQuoter = spawn_symbol("&ready-grid");
    trueConstant = spawn_symbol("true");
    foldConstant = spawn_symbol("&fold-left");
    lambdaConstant = spawn_symbol("&lambda");
    truePredicate = spawn_symbol("&true?");
    remainingArgsConstant = spawn_symbol("&remaining-args");
    localArgsConstant = spawn_symbol("&local-args");
    particulateCar = spawn_symbol("&cons-car");
    particulateCdr = spawn_symbol("&cons-cdr");

    applyProcedureConstant = spawn_symbol("apply");
    pairProcedureConstant = spawn_symbol("pair");
    dotOpConstant = spawn_symbol("contextual-dot-operation");
    redirOpConstant = spawn_symbol("redirect-process-output");

    atomKindConstant = spawn_text("type:symbol");
    errorKindConstant = spawn_text("type:error");
    numKindConstant = spawn_text("type:number");
    textKindConstant = spawn_text("type:text");
    consKindConstant = spawn_text("type:pair");
    listKindConstant = spawn_text("type:list");
    gridKindConstant = spawn_text("type:grid");
    vaukinKindConstant = spawn_text("type:vau");
    genKindConstant = spawn_text("type:gen");

    listConstructorConstant = spawn_symbol("list");

    INS_TO_CTX(ctx, notFoundConstant, assignmentOp);

    ADD_TO_CTX(ctx, "and", truePredicate);
    ADD_TO_CTX(ctx, "contextual-dot-operation", dotOp);
    ADD_TO_CTX(ctx, "def", lambdaConstant);
    ADD_TO_CTX(ctx, "fn", lambdaConstant);
    ADD_TO_CTX(ctx, "fold", foldConstant);
    ADD_TO_CTX(ctx, "foldl", foldConstant);
    ADD_TO_CTX(ctx, "get", dotOp);
    ADD_TO_CTX(ctx, "set!", assignmentOp);
    ADD_TO_CTX(ctx, "text-op", dotOp);
    ADD_TO_CTX(ctx, "true?", truePredicate);

    return ctx;
}

static cell_t *get_state_after_file(FILE *src,
                                    const unsigned int useEval,
                                    const unsigned int showWork,
                                    const char *prompt,
                                    cell_t *context) {
    int lastInput = 0;
    cell_t *got, *result;
    while(lastInput != EOF) {
        if(prompt != NULL)
            printf("%s ", prompt);
        got = comprehend_syntax(read_lisp_chunk(src, &lastInput));
        while(got && got != notFoundConstant) {
            result = ( (useEval) ? eval(context, car(got)) : car(got) );
            if(showWork)
                print(result);
            got = cdr(got);
        }
    }
    return context;
}

static void process_file(FILE *init, FILE *src,
                         const unsigned int useEval,
                         const unsigned int showWork,
                         const char *prompt) {
    cell_t *ctx = (!init) ? initial_context() :
        get_state_after_file(init, 1, 0, NULL, initial_context());
    context_trample(ctx, notFoundConstant, NULL);
    get_state_after_file(src, useEval, showWork, prompt, ctx);
}

void comprehend_file(FILE *src) {
    process_file(NULL, src, 0, 1, NULL);
}

void repl_main(FILE *init, FILE *src, const char *prompt) {
    process_file(init, src, 1, 1, prompt);
}

void run_file(FILE *init, FILE *src) {
    process_file(init, src, 1, 0, NULL);
}
